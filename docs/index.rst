.. LMS documentation master file, created by
   sphinx-quickstart on Mon Feb  5 07:31:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LMS's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/models

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
