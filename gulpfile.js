const gulp = require('gulp');

require('./gulp/less');
require('./gulp/bootstrap');
require('./gulp/watch');
// require('./gulp/image-optimize');

require('./gulp/js-scripts');

gulp.task('default', ['less', 'bootstrap', 'js-scripts']);
