# LMS system

## Development version

1. Install [Docker](https://www.docker.com/) Community Edition not early than 17.09.1-ce
2. Install [git](https://git-scm.com/) not early than 2.10.0
3. [Clone project](https://git-scm.com/docs/git-clone)
4. Login in Docker registry: `docker login registry.gitlab.com` ([see more about docker cli](https://docs.docker.com/engine/reference/commandline/cli/))
10. Run project in docker-environment - `docker-compose up -d`, wait when project starts
([see more about docker-compose](https://docs.docker.com/compose/))
11. Login into container: `docker exec -ti lms_python_1 bash` (container's name may changed, check it in list - `docker ps`,
[see more about docker exec](https://docs.docker.com/engine/reference/commandline/exec/))
12. Install packages from pip: `pip install`
13. DB migration applying: `python ./lms/manage.py migrate`
16. Open website: [http://localhost](http://localhost) (address may changed, depends on environment)


_Creating superuser:_

    docker exec lms_python_1 python /app/lms/manage.py createsuperuser

__Documentation generator:__

    cd docs && make html && cd ../


## Commits naming
- `+` added
- `-` deleted
- `=` some changes in current function
- `!` fixed
- `Х%` completed with X%

## Linter

1. Installation process here [https://pre-commit.com/#install](https://pre-commit.com/#install)
2. Turn on pre-commit for commit messages: `pre-commit install --hook-type commit-msg`
3. Run manually: `pre-commit run --all-files`
4. Enjoy clean code!

## Factories

1. Go to your docker bash: `docker exec -it lms_python_1 bash`
2. Open python's shell using manage.py: `python3 ./lms/manage.py shell`
3. Import AllFactory class: `from website.factories import AllFactory`
4. Create instance of AllFactory specifying amount of every model:
  `factory = AllFactory(author=1, publisher=1, journal=1, document=1, av=1, book=1, article=1)`
5. Generate your models: `factory.generate()`
6. Enjoy your test database!

## Docker interpreter in PyCharm

1. Go to `Settings`->`Build, Execution, Deployment`->`Docker`.
2. Add new with `TCP socket` with `Engine API URL` `unix:///var/run/docker.sock`.
3. Add new project interpreter using `Docker Compose`.
4. Add new `Run Configuration` using newly added interpreter.
5. Enjoy!