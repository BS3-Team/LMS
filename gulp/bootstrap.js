const gulp = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const nano = require('gulp-cssnano');

gulp.task('bootstrap-js', function(cb) {
	return gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
	    'node_modules/bootstrap/dist/js/bootstrap.js',
		'node_modules/handlebars/dist/handlebars.js',
    ])
		.pipe(uglify())
		.pipe(concat('bootstrap.min.js'))
		.pipe(gulp.dest('website/static/dist/'))
});

gulp.task('bootstrap-css', function() {
    return gulp.src([
    	'node_modules/bootstrap/dist/css/bootstrap.css',
    ])
        .pipe(nano())
		.pipe(concat('bootstrap.min.css'))
        .pipe(gulp.dest('website/static/dist/'));
});

gulp.task('bootstrap', ['bootstrap-js', 'bootstrap-css']);
