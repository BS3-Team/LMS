const gulp = require('gulp');
const rename = require('gulp-rename');
const nano = require('gulp-cssnano');
const less = require('gulp-less');

// list of files with path from project root
const fileList = [
    'less/base.less',
];

const tasknames = [];

fileList.forEach(function(file) {
    const resFilename = file.replace(new RegExp('/', 'g'), '-');
    const taskname = 'less-' + resFilename.replace('.less', '');
    tasknames.push(taskname);

    gulp.task(taskname, function(cb) {
        return gulp.src('website/static/' + file)
            .pipe(less())
            .pipe(nano({
                // фикс сжатия z-index - все значения строятся относительно файла, а не глобально.
                // этот флаг выключает данный вид сжатия.
                // https://github.com/ben-eb/gulp-cssnano/issues/8
                zindex: false
            }))
            .pipe(rename(resFilename.replace('.less', '.min.css')))
            .pipe(gulp.dest('website/static/dist/'))
    });
});

// gutil.log('less - loaded tasks', tasknames);

gulp.task('less', tasknames);

module.exports = {
    file_list: fileList,
    watch: function (livereloadCallback) {
        fileList.forEach(function(file) {
            const taskname = 'less-' + file.replace(new RegExp('/', 'g'), '-').replace('.less', '');
            // console.log(file, taskname);

            let tasks = [taskname];

            tasks.push(livereloadCallback);

            gulp.watch('website/static/' + file, tasks);
        });
    }
};
