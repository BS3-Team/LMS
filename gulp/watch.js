const gulp = require('gulp');
const livereload = require('gulp-livereload');
const less_builder = require('./less');

gulp.task('watch', function() {
    livereload.listen();
    const cb = () => livereload.reload();

    less_builder.watch(cb);
});
