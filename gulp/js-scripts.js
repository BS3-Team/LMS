const gulp = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const babel = require('gulp-babel');

gulp.task('scripts-js', function(cb) {
	return gulp.src([
	    'website/static/js/*.js',
    ])
		.pipe(babel({
			presets: ['@babel/env']
		}))
		// .pipe(uglify())
		.pipe(concat('site.min.js'))
		.pipe(gulp.dest('website/static/dist/'))
});


gulp.task('js-scripts', ['scripts-js']);
