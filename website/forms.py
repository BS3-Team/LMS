from django import forms

from .models.document import Document


class DocForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('title', 'published_date', 'copies', 'authors',
                  'is_reference', 'is_bestseller', 'description')
