# Generated by Django 2.0.1 on 2018-02-06 20:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_auto_20180206_2226'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='is_bestseller',
            field=models.BooleanField(default=False),
        ),
    ]
