# Generated by Django 2.0.1 on 2018-02-07 08:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0004_document_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='is_bestseller',
        ),
        migrations.AddField(
            model_name='document',
            name='is_bestseller',
            field=models.BooleanField(default=False),
        ),
    ]
