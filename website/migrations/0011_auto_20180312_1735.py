# Generated by Django 2.0.2 on 2018-03-12 14:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0010_auto_20180312_1734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='record',
            name='status',
            field=models.CharField(choices=[('WG', 'Waiting'), ('TK', 'Taken'), (
                'RN', 'Renewed'), ('RD', 'Returned')], default='WG', max_length=2),
        ),
    ]
