# Generated by Django 2.0.1 on 2018-02-05 15:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=250)),
                ('last_name', models.CharField(max_length=250)),
                ('created_date', models.DateTimeField(
                    default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=250)),
                ('created_date', models.DateTimeField(
                    default=django.utils.timezone.now)),
                ('description', models.TextField()),
                ('published_date', models.DateTimeField(
                    default=django.utils.timezone.now)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Journal',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=250)),
                ('created_date', models.DateTimeField(
                    default=django.utils.timezone.now)),
                ('issue', models.IntegerField()),
                ('published_date', models.DateTimeField(
                    default=django.utils.timezone.now)),
                ('editors', models.ManyToManyField(to='website.Author')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Publisher',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=250)),
                ('created_date', models.DateTimeField(
                    default=django.utils.timezone.now)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Record',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('is_returned', models.BooleanField(default=False)),
                ('is_renewed', models.BooleanField(default=False)),
                ('created_date', models.DateTimeField(
                    default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='AV',
            fields=[
                ('document_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE,
                                                      parent_link=True, primary_key=True, serialize=False, to='website.Document')),
            ],
            options={
                'abstract': False,
            },
            bases=('website.document',),
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('document_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE,
                                                      parent_link=True, primary_key=True, serialize=False, to='website.Document')),
                ('issue', models.IntegerField(null=True)),
                ('publisher', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE,
                                                related_name='book_publisher', to='website.Publisher')),
            ],
            options={
                'abstract': False,
            },
            bases=('website.document',),
        ),
        migrations.CreateModel(
            name='JournalArticles',
            fields=[
                ('document_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE,
                                                      parent_link=True, primary_key=True, serialize=False, to='website.Document')),
            ],
            options={
                'abstract': False,
            },
            bases=('website.document',),
        ),
        migrations.AddField(
            model_name='record',
            name='document',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                    related_name='document', to='website.Document'),
        ),
        migrations.AddField(
            model_name='record',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                    related_name='user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='journal',
            name='publisher',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                    related_name='journal_publisher', to='website.Publisher'),
        ),
        migrations.AddField(
            model_name='document',
            name='authors',
            field=models.ManyToManyField(to='website.Author'),
        ),
        migrations.AddField(
            model_name='journalarticles',
            name='journal',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE,
                                    related_name='journal', to='website.Journal'),
        ),
    ]
