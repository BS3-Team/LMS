from django.conf.urls import url

from .views import book_document, category_view, dashboard_view, document_edit_view, history_view, index_view, \
    manager_view, outstanding_request, renew_document, return_document, search_view, sign_in_view, sign_up_view, \
    sign_out_view, single_view, user_profile_view, api

urlpatterns = [
    url(r'^api/search/', api.api_search, name='api-search'),
    url(r'^$', index_view.index, name='index'),
    url('dashboard', dashboard_view.dashboard_view, name='dashboard'),
    url('history', history_view.history_view, name='history'),
    url('sign-in', sign_in_view.sign_in_view, name='sign-in'),
    url('sign-up', sign_up_view.sign_up_view, name='sign-up'),
    url('sign-out', sign_out_view.sign_out_view, name='sign-out'),
    url('single', single_view.single_view, name='single'),
    url('category', category_view.category_view, name='category'),
    url('user-profile', user_profile_view.user_profile_view, name='user-profile'),
    url(r'^books/(?P<doc_id>\d+)/$', single_view.single_view, name='single-view'),
    url('manager', manager_view.manager_view, name='manager'),
    url(r'^search/$', search_view.search_view, name='search'),
    url(r'^document/(?P<pk>\d+)/edit/$', document_edit_view.document_edit, name='document_edit'),
    url('api/book_document/(?P<doc_id>\d+)/$', book_document.book_document, name='book-document'),
    url('api/return_document/(?P<rec_id>\d+)/$', return_document.return_document, name='return-document'),
    url('api/renew_document/(?P<rec_id>\d+)/$', renew_document.renew_document, name='renew-document'),
    url('api/outstanding_request/(?P<doc_id>\d+)/$', outstanding_request.outstanding_request,
        name='outstanding-request'),

]
