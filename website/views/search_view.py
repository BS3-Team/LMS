from django.db.models import Q
from django.shortcuts import render_to_response, render

from ..models.document import Document, Author


def search_view(request):
    if request.method == "POST":
        input = request.POST['input']
        if input is not None:
            docs = search(input)
        return render_to_response('website/search_results.html', {'docs': docs, 'query': input})
    else:
        return render(request, 'website/index.html')


def search(input):
    docs = Document.objects.filter(title__icontains=input)
    docs = (docs | Document.objects.filter(description__icontains=input))
    authors = Author.objects.filter(Q(first_name__icontains=input) | Q(last_name__icontains=input))
    for auth in authors:
        docs = (docs | Document.objects.filter(author=auth))
    return docs
