from django.db.models import Q
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from website.models.author import Author
from website.models.document import Document

from django.core.serializers import serialize

from website.models.tag import Tag


@csrf_exempt
def api_search(request):
    if request.method == "POST":
        query = request.POST.get('query', '')
        tag = request.POST.get('tag', False)
        tag_id = request.POST.get('tag_id', False)

        if tag:
            tag = Tag.objects.filter(title__icontains=tag).first()
            docs = Document.objects.filter(tags=tag)
            data = serialize('json', docs)

            return HttpResponse(data, content_type='application/json')

        if tag_id:
            tag = Tag.objects.filter(pk=tag_id).first()
            docs = Document.objects.filter(tags=tag)
            data = serialize('json', docs)

            return HttpResponse(data, content_type='application/json')

        if query is not None:

            if 'OR' in query:
                queries = query.split(' OR ')
                docs = Document.objects.filter(Q(title__icontains=queries[0]) | Q(title__icontains=queries[1]))
                authors = Author.objects.filter(Q(first_name__icontains=queries[0]) | Q(last_name__icontains=queries[1]) | Q(first_name__icontains=queries[1]) | Q(last_name__icontains=queries[0]))

                for auth in authors:
                    docs = docs.union(Document.objects.filter(authors=auth))
            elif 'AND' in query:
                queries = query.split(' AND ')
                docs = Document.objects.filter(Q(title__icontains=queries[0]) & Q(title__icontains=queries[1]))
                authors = Author.objects.filter(Q(first_name__icontains=queries[0]) & Q(last_name__icontains=queries[1]) | Q(first_name__icontains=queries[1]) & Q(last_name__icontains=queries[0]))

                for auth in authors:
                    docs = docs.union(Document.objects.filter(authors=auth))
            else:
                docs = Document.objects.filter(title__icontains=query)
                docs = (docs | Document.objects.filter(description__icontains=query))
                authors = Author.objects.filter(Q(first_name__icontains=query) | Q(last_name__icontains=query))

                for auth in authors:
                    docs = docs.union(Document.objects.filter(authors=auth))

        data = serialize('json', docs)

        return HttpResponse(data, content_type='application/json')
    else:
        return HttpResponse(serialize('json', Document.objects.all()), content_type='application/json')
