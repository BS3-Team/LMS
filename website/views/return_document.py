from django.http import HttpResponseServerError
from django.shortcuts import redirect

from ..models.record import Record
from ..models.request import Request


def return_document(request, rec_id=0):
    """Returns document for the user"""
    record = Record.objects.get(pk=rec_id)

    if record is None:
        return HttpResponseServerError()

    request.user.profile.return_document(record.document)
    return redirect('dashboard')
