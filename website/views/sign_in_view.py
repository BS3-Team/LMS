from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, render

from website.utils import is_authenticated


def sign_in_view(request):
    """Render authentication page and redirect after successful login"""
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('dashboard')
        else:
            return redirect('sign-in')
    else:
        if request.user and is_authenticated(request.user):
            return redirect('dashboard')

        return render(request, 'website/sign_in.html')
