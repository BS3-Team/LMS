from django import forms


def editData(request):
    if request.method == 'POST':
        form = forms.ModelForm(request.POST, request.FILES)

    if form.is_valid():
        form.save()