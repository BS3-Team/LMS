from django.db.models import Q
from django.shortcuts import render

from website.models.book import Book
from ..models.document import Document
from ..models.record import Record
from ..models.request import Request


def single_view(request, doc_id=0):
    """Renders single view page of a document"""
    doc = Document.objects.get(pk=doc_id)
    taken_copies = len(Record.objects.filter(Q(document=doc) & (Q(status=Record.TAKEN) | Q(status=Record.RENEWED) | Q(
        status=Record.RETURNED))))
    is_reference = doc.is_reference
    available_copies = doc.copies - taken_copies
    is_requested = Request.objects.filter(document=doc).first() is not None

    # if it is a book, replace model. What is the best approach?
    # @TODO: fix or approve this solution.
    b = Book.objects.filter(pk=doc_id).first()
    if b.pk == doc.pk:
        doc = b

    return render(request, 'website/single.html',
                  {'document': doc, 'taken copies': taken_copies, 'available_copies': available_copies,
                   'is_reference': is_reference, 'is_requested': is_requested})
