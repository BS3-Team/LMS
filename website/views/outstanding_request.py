from django.shortcuts import redirect

from ..models.document import Document
from ..models.request import Request


def outstanding_request(request, doc_id=0):
    document = Document.objects.get(pk=doc_id)
    if request.user.is_staff:
        if len(Request.objects.filter(user=request.user, document=document, is_open=True)) == 0:
            out_req = Request(user=request.user, document=document, type=Request.OUTSTANDING_REQUEST)
            out_req.save()
    return redirect('dashboard')
