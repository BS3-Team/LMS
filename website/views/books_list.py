from django.shortcuts import render

from ..models.document import Document


def books_list(request):
    documents = Document.objects.order_by('created_date')
    return render(request, 'website/books_list.html', {'books': documents})