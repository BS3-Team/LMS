from django.shortcuts import get_object_or_404, redirect, render

from ..forms import DocForm
from ..models.document import Document


def document_edit(request, pk):
    document = get_object_or_404(Document, pk=pk)
    if request.method == "POST":
        form = DocForm(request.POST, instance=document)
        if form.is_valid():
            document.save()
            return redirect('single-view', document.id)
    else:
        form = DocForm(instance=document)
    return render(request, 'website/document_edit.html', {'form': form})