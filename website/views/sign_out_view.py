from django.contrib.auth import logout
from django.shortcuts import redirect


def sign_out_view(request):
    """Sign outs the user and redirects to index page"""
    request.user.save()
    logout(request)
    # log out logic to be added here
    return redirect('index')