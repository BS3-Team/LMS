from django.shortcuts import render, redirect

from ..models.record import Record


def history_view(request):
    """Renders history of check outed books for the user"""
    if request.user.is_authenticated:
        records = Record.objects.all() \
            .filter(user_id=request.user.id).exclude(status=Record.WAITING).order_by('created_date')

        return render(request, 'website/dashboard.html', {'records': records, 'title': "History"})
    else:
        return redirect('sign-in')
