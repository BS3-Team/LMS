from django.views.generic.edit import UpdateView

from lms.website.models.document import Document


class DocumentUpdate(UpdateView):
    model = Document
    fields = 'all'
    template_name_suffix = '_update_form'


