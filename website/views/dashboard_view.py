from django.shortcuts import render, redirect

from ..models.record import Record


def dashboard_view(request):
    """Renders dashboard view or redirects to sign-in if not authenticated"""
    if request.user.is_authenticated:

        records = Record.objects.all() \
            .filter(user_id=request.user.id).exclude(status=Record.CLOSED).order_by('created_date')

        return render(request, 'website/dashboard.html', {'records': records, 'title': "Checked out books"})
    else:
        return redirect('sign-in')
