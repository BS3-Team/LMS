from django.shortcuts import render

from ..models.document import Document


def category_view(request):
    """Renders category page"""
    documents = Document.objects.all()
    return render(request, 'website/category.html', {'documents': documents})