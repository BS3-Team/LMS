from django.shortcuts import redirect

from ..models.record import Record
from ..models.request import Request


def book_document(request, doc_id=0):
    """Books a document (if possible) for the user and redirects to dashboard"""
    record = Record(user_id=request.user.id, document_id=doc_id)

    request.user.profile.checkout_document(record.document)

    return redirect('dashboard')
