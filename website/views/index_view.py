from django.shortcuts import render

from website.models.journal_articles import JournalArticles
from website.models.book import Book
from website.models.av import AV
from website.models.tag import Tag


def index(request):
    """Renders index page"""
    documents = list(Book.objects.order_by('created_date'))
    documents += list(JournalArticles.objects.order_by('created_date'))
    documents += list(AV.objects.order_by('created_date'))

    tags = Tag.objects.all()
    return render(request, 'website/index.html', {'documents': documents, 'tags': tags})
