from django.db.models import Q
from django.shortcuts import render, redirect

from ..models.record import Record


def manager_view(request):
    """Renders manager view for the staff of the library"""
    if request.user.is_staff:
        records = Record.objects.filter(Q(status=Record.WAITING) | Q(status=Record.RETURNED))
        overdue_records = []
        return render(request, 'website/manager.html', {'records': records, 'overdue_records': overdue_records})
    else:
        return redirect('index')