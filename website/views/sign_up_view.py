from django.contrib.auth import authenticate, login
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group, User
from django.shortcuts import redirect, render

from website.utils import is_authenticated


def sign_up_view(request):
    """Render sign up view with registration"""
    if request.method == "POST":
        email = request.POST.get('email', False)
        username = request.POST.get('username', False)
        password1 = request.POST.get('password1', False)
        password2 = request.POST.get('password2', False)

        if password1 == password2:
            student_group = Group.objects.get(name="student")
            new_user = User.objects.create(email=email, username=username,
                                           password=make_password(password1), is_active=False)
            new_user.groups.add(student_group)
            if not new_user.is_active:
                return redirect('sign-in')
            # @TODO: redirect to registration waiting
        user = authenticate(request, username=username, password=password1)
        if user is not None:
            login(request, user)
            return redirect('sign-in')
        else:
            return redirect('sign-up')

    if request.user and is_authenticated(request.user):
        return redirect('dashboard')

    return render(request, 'website/sign_up.html')