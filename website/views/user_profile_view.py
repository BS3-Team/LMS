from django.shortcuts import render


def user_profile_view(request):
    """Renders user profile page"""
    return render(request, 'website/user_profile.html')