from django.shortcuts import redirect

from ..models.record import Record


def renew_document(request, rec_id=0):
    """Sends renew request for the manager"""
    record = Record.objects.get(pk=rec_id)

    request.user.profile.renew_document(record.document)

    return redirect('dashboard')
