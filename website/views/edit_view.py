from django.shortcuts import render

from ..models.document import Document


def edit_view(request, pk):
    if request.method == "POST":
        title = request.POST['title']
        is_bestseller = request.POST['is_bestseller']
        is_reference = request.POST['is_reference']
        copies = request.POST['copies']
        description = request.POST['description']
        publish_date = request.POST['publish_date']
        doc = Document.objects.get(pk=pk)
        if not title == "":
            doc.title = title
        if not description == "":
            doc.description = description
        doc.publish_date = publish_date
        doc.copies = copies
        doc.is_reference = is_reference
        doc.is_bestseller = is_bestseller
    return render(request, 'website/document_edit.html')