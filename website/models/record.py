import datetime
from datetime import timedelta

import pytz
from django.db import models
from django.utils import timezone

from .request import Request


class Record(models.Model):
    """Relationship between users and documents. Creates when user takes a document."""
    user = models.ForeignKey(
        'auth.User', related_name='user', on_delete=models.CASCADE)
    document = models.ForeignKey(
        'Document', related_name='document', on_delete=models.CASCADE)
    is_highest = models.BooleanField(default=False)
    created_date = models.DateTimeField(default=timezone.now)

    last_status_modified_date = models.DateTimeField(default=timezone.now)

    WAITING = 'WG'
    EXPIRED = 'EX'
    DEFERRED = 'DF'
    TAKEN = 'TK'
    RENEWED = 'RN'
    RETURNED = 'RD'
    CLOSED = 'CL'

    STATUS_TYPES = (
        (WAITING, 'Waiting'),
        (EXPIRED, 'Expired'),
        (DEFERRED, 'Deferred'),
        (TAKEN, 'Taken'),
        (RENEWED, 'Renewed'),
        (RETURNED, 'Returned'),
        (CLOSED, 'Closed')
    )

    status = models.CharField(max_length=2, choices=STATUS_TYPES, default=WAITING)

    WEEK_INTERVALS = {
        1: timedelta(days=7),
        2: timedelta(days=14),
        3: timedelta(days=21),
        4: timedelta(days=28)
    }

    def __init__(self, *args, **kwargs):
        super(Record, self).__init__(*args, **kwargs)
        self.old_state = self

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        """Update last_status_modified_date only in case of changed status"""
        if self.old_state.status != self.status:
            self.last_status_modified_date = timezone.now()
        super(Record, self).save(force_insert, force_update, *args, **kwargs)

    def get_fine(self, current_date=datetime.datetime.now(tz=pytz.UTC)):
        """Method to check the fine for current record"""
        return min(self.get_overdue_days(current_date) * 100, self.document.price)

    def get_overdue_days(self, current_date=datetime.datetime.now(tz=pytz.UTC)):
        """Method to check overdue days for the current record"""
        return_date = self.get_return_date()
        if return_date < current_date and self.status != Record.RETURNED:
            return (current_date - return_date).days
        else:
            return 0

    def get_return_date(self):
        """Returns return date for the document"""

        outstanding_request = Request.objects.filter(
            is_open=True, type=Request.OUTSTANDING_REQUEST, document=self.document).first()
        if outstanding_request is not None:
            return outstanding_request.created_date

        calculating_date = self.last_status_modified_date

        if self.user.profile.is_visiting_professor:
            return calculating_date + Record.WEEK_INTERVALS.get(1)

        # Check for AV and Articles
        if self.document.type == 'AV' or self.document.type == 'AC':
            return calculating_date + Record.WEEK_INTERVALS.get(2)

        # Check for bestseller
        if self.document.is_bestseller and self.user.profile.is_student:
            return calculating_date + Record.WEEK_INTERVALS.get(2)

        if self.user.profile.is_faculty:
            return calculating_date + Record.WEEK_INTERVALS.get(4)
        else:
            return calculating_date + Record.WEEK_INTERVALS.get(3)

    def __str__(self):
        return '"' + self.document.title + '" is taken by ' + \
               self.user.first_name + ' ' + self.user.last_name + \
               ' on ' + self.created_date.strftime("%d.%m.%Y")

    class Meta:
        app_label = 'website'
