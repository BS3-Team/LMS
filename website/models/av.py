from .document import Document


class AV(Document):
    """Audio/Video model is one of Document types"""
    type = Document.AV