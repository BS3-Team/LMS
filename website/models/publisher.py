from .base_model import BaseModel


class Publisher(BaseModel):
    """Publisher is a company which publish some Documents"""
    def __str__(self):
        return self.title
    pass