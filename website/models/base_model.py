from django.db import models
from django.utils import timezone


class BaseModel(models.Model):
    """Base model implements the most popular stuff"""
    title = models.CharField(max_length=250)
    created_date = models.DateTimeField(
        default=timezone.now)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True
        app_label = 'website'