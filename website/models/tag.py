from django.db import models
from django.utils import timezone

from .base_model import BaseModel


class Tag(BaseModel):
    """Model of tags and keywords for documents """
    pass
