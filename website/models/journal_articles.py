from django.db import models

from .document import Document


class JournalArticles(Document):
    """JournalArticles is one of Document types"""
    journal = models.ForeignKey(
        'website.Journal', related_name='journal', on_delete=models.CASCADE, null=True)
    type = Document.ARTICLES
