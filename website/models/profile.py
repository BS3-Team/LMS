from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import gettext as _

from website.models.record import Record
from website.models.request import Request


class Profile(models.Model):
    """Model Profile is an extension of model User"""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.TextField(max_length=150, blank=True)

    phone_regex = RegexValidator(regex=r'^\d{9,12}$',
                                 message=_("Phone number must be entered in the format: +79999999999"))
    phone_number = models.CharField(
        verbose_name=_('phone number'), validators=[phone_regex], max_length=13, null=True, unique=True)

    STUDENT = 1
    INSTRUCTOR = 2
    TEACHING_ASSISTANT = 3
    VISITING_PROFESSOR = 4
    PROFESSOR = 5
    LIBRARIAN = 6

    TYPE_TYPES = (
        (STUDENT, 'Student'),
        (INSTRUCTOR, 'Instructor'),
        (TEACHING_ASSISTANT, 'TA'),
        (VISITING_PROFESSOR, 'Visitng Professor'),
        (PROFESSOR, 'Professor'),
        (LIBRARIAN, 'Librarian')
    )

    type = models.IntegerField(choices=TYPE_TYPES, default=STUDENT)

    def __str__(self):
        """String representations of the Profile"""
        return self.user.first_name + ' ' + self.user.last_name

    @property
    def is_student(self):
        """Virtual attribute to check if the profile corresponds to the group 'Student' """
        return self.user.groups.filter(name='student').exists()

    @property
    def is_faculty(self):
        """Virtual attribute to check if the profile corresponds to the group 'Faculty' """
        return self.user.groups.filter(name='faculty').exists()

    @property
    def is_librarian(self):
        """Virtual attribute to check if the profile corresponds to the group 'Librarian' """
        return self.user.groups.filter(name='librarian').exists()

    @property
    def is_visiting_professor(self):
        """Virtual attribute to check if the profile corresponds to the group 'Visiting Professor' """
        return self.user.groups.filter(name="visiting professor").exists()

    def checkout_document(self, document):
        """Method to checkout a document

            Returns:    i.  False if the document cannot be checked out
                        ii. Newly create model Record instance if the document is checked out
        """

        # Q objects are used to construct argument for filter in db query
        q = Q(document=document, user=self.user) & (
                Q(status=Record.WAITING) | Q(
            status=Record.TAKEN) | Q(status=Record.RENEWED) | Q(status=Record.RETURNED))
        not_closed_yet = Record.objects.filter(q).exists()

        outstanding_request = Request.objects.filter(document=document, type=Request.OUTSTANDING_REQUEST).exists()
        if document.is_reference or outstanding_request or not_closed_yet:
            return False

        record = Record(user=self.user, document=document)
        record.save()

        # Close all BOOK_AVAILBALE requests on the current document
        for request in Request.objects.filter(user=self.user, document=document, type=Request.BOOK_AVAILABLE):
            request.is_open = False

        return record

    def return_document(self, document):
        """
            Method for returning the document
            If returned successfully status of the record changes to RETURNED
            The status of approved returned book changes to CLOSED
        """
        record = Record.objects.all().filter(
            Q(user=self.user, document=document, status=Record.TAKEN) | Q(user=self.user, document=document,
                                                                          status=Record.RENEWED)).first()

        if record is None:
            return False

        record.status = Record.RETURNED
        record.save()

        requests = Request.objects.filter(
            user=self.user, document=document)
        for request in requests:
            request.is_open = False

        return True

    def renew_document(self, document):
        """
            Method for renewing the document
            If returned successfully status of the record changes to RENEWED
        """

        # Construction of Q object for DB query which extracts only records able to be renewed
        # Consider all records with status TAKEN
        q = Q(status=Record.TAKEN)
        # For visiting professor type also add records with status RENEWED
        if self.is_visiting_professor:
            q = q | Q(status=Record.RENEWED)
        # Now we extract all records which are related to current document and user
        q = q & Q(document=document, user=self.user)

        # Consider an element from QuerySet
        record = Record.objects.all().filter(q).first()

        outstanding_request_exists = Request.objects.filter(document=document,
                                                            type=Request.OUTSTANDING_REQUEST).exists()

        if record is None or outstanding_request_exists:
            return False

        record.status = Record.RENEWED
        record.last_status_modified_date = timezone.now()
        record.save()
        return True
