from django.db import models
from django.utils import timezone


class Author(models.Model):
    """Author model which stores information about """

    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    created_date = models.DateTimeField(
        default=timezone.now)

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        app_label = 'website'