from django.db import models
from django.utils import timezone

from .author import Author
from .base_model import BaseModel


class Journal(BaseModel):
    """Journal use as a publish place in JournalArticles"""
    publisher = models.ForeignKey(
        'website.Publisher', related_name='journal_publisher', on_delete=models.CASCADE)
    issue = models.IntegerField()
    ISSN = models.CharField(max_length=17, default="")
    pages = models.IntegerField(null=True)
    editors = models.ManyToManyField(Author)
    published_date = models.DateTimeField(
        default=timezone.now)
