from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.utils import timezone

from .record import Record
from .tag import Tag
from .author import Author
from .base_model import BaseModel


class Document(BaseModel):
    """Document model which is used for all types of documents stored in library."""
    is_reference = models.BooleanField(default=False)
    is_bestseller = models.BooleanField(default=False)
    authors = models.ManyToManyField(Author)
    tags = models.ManyToManyField(Tag)
    price = models.IntegerField(default=0)
    copies = models.IntegerField(default=0)
    description = models.TextField()
    published_date = models.DateTimeField(
        default=timezone.now)

    BOOK = 'BK'
    AV = 'AV'
    ARTICLES = 'AC'

    DOC_TYPES = (
        (BOOK, 'Book'),
        (AV, 'av'),
        (ARTICLES, 'JournalArticles'),
    )

    type = models.CharField(max_length=2, choices=DOC_TYPES, default=BOOK)

    def clean(self):
        """
            Validation for copies of check outed documents
        """
        q = Q(document=self) & (Q(status=Record.TAKEN) | Q(status=Record.RENEWED) | Q(status=Record.RETURNED))
        records = Record.objects.all().filter(q)
        if len(records) > self.copies:
            raise ValidationError(
                'Count of copies cannot be smaller than count of check outed books.')

    @property
    def img_src(self):
        """Virtual attribute for image source string"""
        return '/static/images/products/products-3.jpg'
