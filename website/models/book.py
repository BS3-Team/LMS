from django.db import models

from .document import Document


class Book(Document):
    """Books is one of Document types"""
    publisher = models.ForeignKey('website.Publisher', related_name='book_publisher', on_delete=models.CASCADE,
                                  null=True)
    issue = models.IntegerField(null=True)
    ISBN = models.CharField(max_length=17, default="")
    pages = models.IntegerField(null=True)
    type = Document.BOOK

    @property
    def reading_time_in_hours(self):
        # average speed of reading is 45-50 pages per hour
        return self.pages // 50 + 1
