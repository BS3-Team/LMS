from django.db import models
from django.utils import timezone


class Request(models.Model):
    """Request model for requesting a document"""
    user = models.ForeignKey(
        'auth.User', related_name="requests_user", on_delete=models.CASCADE)
    document = models.ForeignKey(
        'Document', related_name="requests_document", on_delete=models.CASCADE)
    is_open = models.BooleanField(default=True)
    is_sent = models.BooleanField(default=False)
    created_date = models.DateTimeField(
        default=timezone.now)
    BOOK_AVAILABLE = 'BA'
    BOOK_REQUEST = 'BR'
    OUTSTANDING_REQUEST = 'OR'
    RETURN_BOOK = 'RB'
    DEFERRED_BOOK = 'DB'
    REQUEST_TYPES = (
        (BOOK_AVAILABLE, 'Book Available'),
        (BOOK_REQUEST, 'Book Request'),
        (OUTSTANDING_REQUEST, 'Outstanding Request'),
        (RETURN_BOOK, 'Return Book'),
        (DEFERRED_BOOK, 'Deferred Book')
    )

    def __str__(self):
        return "{} created by {} on document {} ".format(self.get_type_display(), self.user, self.document)

    type = models.CharField(max_length=2, choices=REQUEST_TYPES, default=BOOK_AVAILABLE)
