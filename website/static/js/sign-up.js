$(document).ready(() => {
    $('.form-signin input').on('change', (e) => {
        let $input = $(e.target);
        let val = $input.val();

        if (val.length > 0) {
            $input.removeClass('is-invalid');
        } else {
            $input.removeClass('is-valid');
            $input.addClass('is-invalid');
        }
    });

    $(".form-signin").submit((e) => {

        $(".form-signin input").each(function (index, element) {
            let $element = $(element);

            if (!$element.val()) {
                e.preventDefault();
            }
        });

    });

});
