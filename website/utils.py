import datetime
import pytz
from django.contrib.auth.models import User
from django.db.models import Q

from website.models.author import Author
from website.models.tag import Tag
from .models.profile import Profile
from .models.record import Record
from .models.document import Document


def is_access_level_1(user):
    """Method to check if the user has access level 1"""
    return user.groups.filter(name="Access Level 1").exists()


def is_access_level_2(user):
    """Method to check if the user has access level 2"""
    return user.groups.filter(name="Access Level 2").exists()


def is_access_level_3(user):
    """Method to check if the user has access level 3"""
    return user.groups.filter(name="Access Level 3").exists()


# Method to check if model was created.
def test_model_created(model):
    """
    if record.pk is None:
            # Definitely doesn't exist, since there's no `pk`.
            exists = False
        else:
            # The `pk` is set, but it doesn't guarantee exists in db.
            try:
                obj_from_db = MyModel.objects.get(pk=obj.pk)
                exists = True
            except MyModel.DoesNotExist:
                exists = False

        return exists

    return test()
    """
    return model.pk is not None


def documents_copies_number(models):
    """Method to check the copies number of a particular document"""
    n = 0
    for i in models:
        n += i.copies
    return n


def get_overdued_records(records, date=datetime.datetime.now(pytz.UTC)):
    """Method to check overdued records from an iterable of records"""
    res = []
    for r in records:
        if r.get_overdue_days(date):
            res.append(r)

    return res


def get_fines(records, date=datetime.datetime.now(pytz.UTC)):
    """Method to ckeck fines of an iterable of records"""
    records = get_overdued_records(records)

    fines = [(date - rec.get_return_date(), rec.get_fine(date))
             for rec in records]

    return fines


def get_waiting_list(records, from_highest=True):
    """Method to check waiting list constructed from an iterable of records"""
    if from_highest:
        return [o for o in
                records.filter(is_highest=True, status=Record.WAITING).order_by('-user__profile__type')] + \
               [o for o in
                records.filter(is_highest=False, status=Record.WAITING).order_by('-user__profile__type')]
    else:
        return [o for o in
                records.filter(is_highest=False, status=Record.WAITING).order_by('user__profile__type')] + \
               [o for o in
                records.filter(is_highest=True, status=Record.WAITING).order_by('user__profile__type')]


def get_all_records(object):
    """ Returns a QuerySet of all records related to the object
            Scenarios:
                1. object is document
                2. object is user
                3. object is profile
    """
    if issubclass(type(object), Document):
        records = Record.objects.all().filter(document=object)
        return records

    if type(object) is User:
        records = Record.objects.all().filter(user=object)
        return records

    if type(object) is Profile:
        records = Record.objects.all().filter(user=object.user)
        return records


def all_records_closed(object):
    """ Checks if all records related to the object are closed
        Scenarios:
            1. object is document
            2. object is user
            3. object is profile
    """
    not_closed_records = get_all_records(object).exclude(status="CL")
    return len(not_closed_records) == 0


def is_authenticated(user):
    """Method to check if the user is authenticated"""
    if callable(user.is_authenticated):
        return user.is_authenticated()
    return user.is_authenticated


def search_helper(input, tag=None):
    """Search helper method for optimization"""
    if tag:
        tag = Tag.objects.filter(title__icontains=tag).first()
        docs = Document.objects.filter(tags=tag)
    else:
        if 'OR' in input:
            queries = input.split(' OR ')
            docs = Document.objects.filter(Q(title__icontains=queries[0]) | Q(title__icontains=queries[1]))
            authors = Author.objects.filter(
                Q(first_name__icontains=queries[0]) | Q(last_name__icontains=queries[1]) | Q(
                    first_name__icontains=queries[1]) | Q(last_name__icontains=queries[0]))

            for auth in authors:
                docs = docs.union(Document.objects.filter(authors=auth))
        elif 'AND' in input:
            queries = input.split(' AND ')
            docs = Document.objects.filter(Q(title__icontains=queries[0]) & Q(title__icontains=queries[1]))
            authors = Author.objects.filter(
                Q(first_name__icontains=queries[0]) & Q(last_name__icontains=queries[1]) | Q(
                    first_name__icontains=queries[1]) & Q(last_name__icontains=queries[0]))

            for auth in authors:
                docs = docs.union(Document.objects.filter(authors=auth))
        else:
            docs = Document.objects.filter(title__icontains=input)
            docs = (docs | Document.objects.filter(description__icontains=input))
            authors = Author.objects.filter(Q(first_name__icontains=input) | Q(last_name__icontains=input))

            for auth in authors:
                docs = docs.union(Document.objects.filter(authors=auth))

    return docs
