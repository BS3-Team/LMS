from datetime import datetime, timedelta
import factory
import random

import pytz

from .models.author import Author
from .models.av import AV
from .models.base_model import BaseModel
from .models.book import Book
from .models.journal import Journal
from .models.journal_articles import JournalArticles
from .models.publisher import Publisher
from .models.document import Document

"""
Factory is used to generate objects of our models in database so we could test them (models)
One of the ways to do it is open your docker bash
$ docker exec -it lms_python_1 bash

There you need to open django shell
$ ./lms/manage.py shell

In shell import this module (from website.factories import *) and use any of this classes


!!!
Problem with many-to-many
When calling UserFactory() or UserFactory.build(), no group binding will be created.
But when UserFactory.create(groups=(group1, group2, group3)) is called,
the groups declaration will add passed in groups to the set of groups for the user.
!!!
"""


class BaseModelFactory(factory.django.DjangoModelFactory):
    """Factory for the base model"""

    class Meta:
        model = BaseModel
        abstract = True

    title = factory.Faker('company')
    created_date = datetime.now(pytz.utc)


class AuthorFactory(factory.django.DjangoModelFactory):
    """Factory for the author model"""

    class Meta:
        model = Author

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    created_date = datetime.now(pytz.utc)


class PublisherFactory(BaseModelFactory):
    """Factory for the publsiher"""

    class Meta:
        model = Publisher


class JournalFactory(BaseModelFactory):
    """Factory for the journal"""

    class Meta:
        model = Journal

    publisher = factory.SubFactory(PublisherFactory)
    issue = random.randint(0, 20)
    editors = factory.SubFactory(AuthorFactory)
    published_date = datetime.now(
        pytz.utc) - timedelta(365 * random.randint(1, 10))

    @factory.post_generation
    def editors(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for editor in extracted:
                self.editors.set(editor)


class DocumentFactory(BaseModelFactory):
    """Factory for the document model"""

    class Meta:
        model = Document

    is_reference = False
    is_bestseller = bool(random.randint(0, 1))
    authors = factory.SubFactory(AuthorFactory)
    copies = random.randint(1, 20)
    description = factory.Sequence(lambda n: 'description%d' % n)
    published_date = datetime.now(
        pytz.utc) - timedelta(365 * random.randint(1, 10))
    type = factory.Iterator(['BK', 'AV', 'AC'])

    @factory.post_generation
    def authors(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for author in extracted:
                self.authors.set(author)


class AVFactory(DocumentFactory):
    """Factory for the audio video model"""

    class Meta:
        model = AV

    type = 'AV'


class BookFactory(DocumentFactory):
    """Factory for the book model"""

    class Meta:
        model = Book

    issue = copies = random.randint(1, 10)
    publisher = factory.SubFactory(PublisherFactory)
    type = 'BK'


class JournalArticlesFactory(DocumentFactory):
    """Factory for the journal articles model"""

    class Meta:
        model = JournalArticles

    journal = factory.SubFactory(JournalFactory)
    type = 'AC'


class AllFactory:
    """Convenient factory for every model"""

    def __init__(self, author=0, publisher=0, journal=0, document=0, av=0, book=0, article=0):
        """Sets the number of every object model"""
        self.author = author
        self.publisher = publisher
        self.journal = journal
        self.document = document
        self.av = av
        self.book = book
        self.article = article

    def generate(self):
        """Generates all objects"""
        for i in range(self.author):
            a = AuthorFactory()
            print("Author " + str(a) + " is created")

        for i in range(self.publisher):
            a = PublisherFactory()
            print("Publisher " + str(a) + " is created")
        authors = Author.objects.all()

        for i in range(self.journal):
            journ = JournalFactory()
            if authors.first is None:
                AuthorFactory()
            journ.editors.set([authors[random.randint(0, len(authors) - 1)]])
            print("Journal " + str(journ) + " is created")

        for i in range(self.document):
            doc = DocumentFactory()
            if authors.first is None:
                AuthorFactory()
            doc.authors.set([authors[random.randint(0, len(authors) - 1)]])
            print("Document " + str(doc) + " is created")

        for i in range(self.av):
            au_vid = AVFactory()
            if authors.first is None:
                AuthorFactory()
            au_vid.authors.set([authors[random.randint(0, len(authors) - 1)]])
            print("AV " + str(au_vid) + " is created")

        for i in range(self.book):
            books = BookFactory()
            if authors.first is None:
                AuthorFactory()
            books.authors.set([authors[random.randint(0, len(authors) - 1)]])
            print("Book " + str(books) + " is created")

        for i in range(self.article):
            ja = JournalArticlesFactory()
            if authors.first is None:
                AuthorFactory()
            ja.authors.set([authors[random.randint(0, len(authors) - 1)]])
            print("Journal Article " + str(ja) + " is created")
