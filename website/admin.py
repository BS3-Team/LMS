from django.contrib import admin, messages
import datetime
import pytz
from django.contrib.admin import SimpleListFilter
from django.contrib.auth import get_permission_codename
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.db.models import Case, When
from django.urls import reverse, NoReverseMatch
from django.utils.html import format_html
from django.template.defaultfilters import escape
from django.contrib import admin
from django.contrib.admin.models import LogEntry

from .models.tag import Tag
from .utils import get_waiting_list, all_records_closed

from .models.request import Request
from .models.profile import Profile
from .models.journal_articles import JournalArticles
from .models.book import Book
from .models.av import AV
from .models.journal import Journal
from .models.publisher import Publisher
from .models.author import Author
from .models.record import Record

from templated_email import get_templated_mail

admin.site.register(AV)
admin.site.register(JournalArticles)
admin.site.register(Publisher)
admin.site.register(Author)
admin.site.register(Journal)
admin.site.register(Request)
admin.site.register(Tag)


class ActionFlagFilter(SimpleListFilter):
    """Filter for actions flag"""
    title = 'action'

    parameter_name = 'action_flag'

    def lookups(self, request, model_admin):
        return (
            ('Addition', 'Addition'),
            ('Change', 'Change'),
            ('Deletion', 'Deletion')
        )

    def queryset(self, request, queryset):
        if self.value() == 'Addition':
            return queryset.filter(action_flag=1)
        elif self.value() == 'Change':
            return queryset.filter(action_flag=2)
        elif self.value() == 'Deletion':
            return queryset.filter(action_flag=3)
        else:
            return queryset


class ReturnDateFilter(SimpleListFilter):
    """Filter for return date"""
    title = 'return date'

    parameter_name = 'get_return_date'

    def lookups(self, request, model_admin):
        return (
            ('Overdue', 'Overdue'),
            ('Good Standing', 'Good Standing')
        )

    def queryset(self, request, queryset):
        q_ids = [o.id for o in queryset if o.get_return_date() >
                 datetime.datetime.now(tz=pytz.UTC)]
        if self.value() == 'Good Standing':
            return queryset.filter(id__in=q_ids)
        elif self.value() == 'Overdue':
            return queryset.filter().exclude(id__in=q_ids)
        else:
            return queryset


class WaitingFilter(SimpleListFilter):
    """Filter for list of waiting"""

    title = 'Waiting list by priority'

    parameter_name = 'status'

    def lookups(self, request, model_admin):
        return (
            ('highest', 'Highest'),
            ('lowest', 'Lowest')
        )

    def queryset(self, request, queryset):
        # We need to check if there is any patron who have been notified, but haven't came for the item
        for record in queryset.filter(status='WG'):
            for request in Request.objects.filter(user=record.user, document=record.document, is_open=True, type='BA'):
                if request.created_date < datetime.datetime.now(tz=pytz.UTC) - datetime.timedelta(days=1):
                    user = record.user
                    document = record.document
                    message = get_templated_mail(
                        template_name='late_to_take_the_book',
                        from_email='systemlibrary@yandex.ru',
                        to=[user.email],
                        context={
                            'first_name': user.first_name,
                            'last_name': user.last_name,
                            'book_name': document.title,
                            'authors': ', '.join([i.__str__() for i in document.authors.all()])
                        },
                    )
                    message.send(fail_silently=False)
                    request.is_open = False
                    record.status = 'EX'
                    record.save()
                    request.save()
        # Then proceed normally
        pk_ordered = [o.id for o in queryset]
        if self.value() == 'lowest':
            pk_ordered = get_waiting_list(queryset, False)
            pk_ordered = [o.id for o in pk_ordered]
        elif self.value() == 'highest':
            pk_ordered = get_waiting_list(queryset, True)
            pk_ordered = [o.id for o in pk_ordered]

        preserved = Case(*[When(pk=pk, then=pos)
                           for pos, pk in enumerate(pk_ordered)])
        return queryset.filter(id__in=pk_ordered).order_by(preserved)


@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'

    list_display = ('id', 'document_link', 'user_link', 'created_date',
                    'return_date', 'fine', 'phone_number', 'status')

    list_display_links = ('id',)

    def user_link(self, obj):
        return format_html(
            '<a target="_blank" href="{}">{}</a>',
            reverse("admin:auth_user_change", args=(obj.user.id,)),
            escape(obj.user.profile)
        )

    user_link.allow_tags = True
    user_link.short_description = "User"

    def document_link(self, obj):
        return format_html(
            '<a target="_blank" href="{}">{}</a>',
            reverse("admin:website_" + dict(obj.document.DOC_TYPES).get(
                obj.document.type).lower() + "_change", args=(obj.document.id,)),
            escape(obj.document)
        )

    document_link.allow_tags = True
    document_link.short_description = "Document"

    def phone_number(self, obj):
        return obj.user.profile.phone_number

    phone_number.allow_tags = True
    phone_number.short_description = 'Phone number'

    overdue_color_code = 'ff0000'

    def fine(self, obj):
        fine = obj.get_fine()
        return fine

    def return_date(self, obj):
        return_date = obj.get_return_date()
        if datetime.datetime.now(tz=pytz.UTC) < return_date or obj.status == 'RD':
            return return_date
        else:
            return_date_str = return_date.strftime("%B %d, %Y, ")
            if return_date.strftime("%I")[0] == '0':
                return_date_str += return_date.strftime("%I")[1]
            return_date_str += return_date.strftime(":%M ")
            for i in return_date.strftime("%p").lower():
                return_date_str += i + "."
        return format_html(
            '<span style="color: #{};">{}</span>',
            self.overdue_color_code,
            return_date_str
        )

    return_date.short_desription = 'Return date'
    list_filter = (
        ('user', admin.RelatedFieldListFilter),
        ('document', admin.RelatedFieldListFilter),
        ('user__profile__phone_number', admin.AllValuesFieldListFilter),
        ReturnDateFilter,
        ('status', admin.ChoicesFieldListFilter),
        WaitingFilter
    )
    search_fields = (
        'user__username',
        'user__first_name',
        'user__last_name',
        'document__title'
    )


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'description',
                    'published_date', 'is_all_returned')

    def is_all_returned(self, obj):
        return all_records_closed(obj)

    def delete_model(self, request, obj):
        if not all_records_closed(obj):
            storage = messages.get_messages(request)
            storage.used = True
            storage = messages.get_messages(request)
            for message in storage:
                message.message = "Test"
            messages.error(
                request, 'Cannot delete book with not returned instances.')
        else:
            obj.delete()

    def get_actions(self, request):
        actions = super(BookAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    search_fields = (
        'title',
        'description'
    )


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'


class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline,)

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)

    def delete_model(self, request, obj):
        if not all_records_closed(obj.profile):
            messages.error(
                request, 'Cannot delete user with not returned documents.')
        elif not request.user.is_superuser and (obj.profile.is_librarian or obj.is_superuser):
            messages.error(request, 'You cannot modify other librarians')
        else:
            obj.delete()

    def has_change_permission(self, request, obj=None):
        if obj is None:
            return True
        if obj is None:
            is_staff = False
        else:
            is_staff = obj.profile.is_librarian or obj.is_superuser

        opts = self.opts
        codename = get_permission_codename('change', opts)
        return (request.method in ['GET', 'HEAD', 'POST'] and
                request.user.has_perm("%s.%s" % (opts.app_label, codename)) and
                (request.user.is_superuser or not is_staff) or
                request.user == obj)

    def has_delete_permission(self, request, obj=None):
        list_of_ids = request.POST.getlist('_selected_action')
        list_of_users = []
        for id in list_of_ids:
            list_of_users.append(User.objects.get(pk=int(id)))
        is_staff = False
        for user in list_of_users:
            is_staff = is_staff or (user.profile.is_librarian or user.is_superuser)
        opts = self.opts
        codename = get_permission_codename('delete', opts)
        return (request.user.has_perm("%s.%s" % (opts.app_label, codename)) and
                (request.user.is_superuser or not is_staff))

    def save_model(self, request, obj, form, change):
        """Redefinition of the save model"""
        superuser_queryset = User.objects.filter(is_superuser=True)
        # Basically we check if new object is superuser and it is not one, that already exists
        # If it's true, we display error message and force is_superuser to False
        if (obj.is_superuser and superuser_queryset.first() != obj) \
                and superuser_queryset.exists():
            messages.error(
                request, 'Cannot create another superuser')
            obj.is_superuser = False
        super().save_model(request, obj, form, change)


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)


class LogEntryAdmin(admin.ModelAdmin):
    actions = None
    date_hierarchy = 'action_time'
    list_per_page = 20

    list_display = ('action_time', 'user_link', 'type', 'object_link', 'changes', 'action')

    search_fields = (
        'object_repr',
        'change_message'
    )

    list_filter = (
        'user',
        'content_type',
        ActionFlagFilter
    )

    def action(self, obj):
        action_flag = obj.action_flag
        if action_flag == 1:
            return "Addition"
        elif action_flag == 3:
            return "Deletion"
        else:
            return "Change"

    action.short_description = "Action"

    def changes(self, obj):
        return obj.get_change_message()

    changes.short_description = "Changes"

    def object_link(self, obj):
        return format_html('<a target="_blank" href="{}">{}</a>',
                           obj.get_admin_url(),
                           escape(obj.object_repr)
                           )

    object_link.allow_tags = True
    object_link.short_description = "Object"

    def type(self, obj):
        content_type_name = str(obj.content_type)
        return content_type_name[:1].upper() + content_type_name[1:].lower()

    type.short_description = "Content Type"

    def user_link(self, obj):
        return format_html(
            '<a target="_blank" href="{}">{}</a>',
            reverse("admin:auth_user_change", args=(obj.user.id,)),
            escape(obj.user.profile)
        )

    user_link.allow_tags = True
    user_link.short_description = "User"

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['readonly'] = True
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super(LogEntryAdmin, self).change_view(request, object_id, form_url='', extra_context=extra_context)

    def get_readonly_fields(self, request, obj=None):
        return self.fields or [f.name for f in self.model._meta.fields]

    def has_add_permission(self, rfgequest):
        return False

    # Allow viewing objects but not actually changing them.
    def has_change_permission(self, request, obj=None):
        return (request.method in ['GET', 'HEAD'] and
                super().has_change_permission(request, obj))

    def has_delete_permission(self, request, obj=None):
        return False

    def has_module_permission(self, request):
        return request.user.is_superuser


admin.site.register(LogEntry, LogEntryAdmin)
