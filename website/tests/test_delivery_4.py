import json

import pytz
import requests
from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import User, Group
from django.core import mail
from django.test import TestCase
from django.utils import timezone
import datetime

from website.models.tag import Tag
from ..models.author import Author
from ..models.publisher import Publisher
from ..models.book import Book
from ..models.av import AV
from ..models.document import Document
from ..models.record import Record
from ..models.request import Request

from ..utils import documents_copies_number, get_overdued_records, get_waiting_list, search_helper
from ..views.search_view import search

"""
All tests should be in package named (module_name)/tests/...
Django will automatically start every test which starts with test*.py
And execute every test which starts with "test"

To start your tests you need to write
"./manage.py test *name_of_testings*" in your docker's bash
(To enter docker bash write in main directory "docker exec -it lms_python_1 bash")
Sometimes you need to explicitly write
$  python3 ./manage.py test

For some reason django couldn't find tests in website so you need to write
$  manage.py test website.tests

To test something specifically write
$  ./manage.py test website.tests.*class_name*.*test_name*


You can specify a custom filename pattern match using the -p (or --pattern) option,
if your test files are named differently from the test*.py pattern:

$ ./manage.py test --pattern="tests_*.py"

You can prevent the test databases from being destroyed by using the "test --keepdb" option.

As long as your tests are properly isolated,
you can run them in parallel to gain a speed up on multi-core hardware. Use test --parallel.

Functions used
setUpTestData: Run once to set up non-modified data for all class methods.
setUp:  Run once for every test method to setup clean data.
tearDown:  Clean up run after every test method.

All tests should be named this way:
[test_MethodName_StateUnderTest_ExpectedBehavior]
If you want your test to run with other you MUST write "test" at the beginning of the your test


The idea of naming them like this was took from Roy Osherove
Source: http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html


Each test should be described in format:
Test Case 1
    Initial state:      p1 checked out d1 and d2 on March 5th
    Action:             I.  p1 returns d2
                        II. librarian checks the dues and fines of p1
    Expected Result:    Overdue: [(d1, 0 days]
                        Fine: [(0 days, 0 rub)]


"""


class TestSuiteDelivery4(TestCase):
    """This module has unit tests specified in test cases for delivery #3"""

    def setUp(self):
        """
        SETUP
        System has one admin 'admin1'
        """
        User.objects.create(username='root', is_superuser=True)

    def test_attempt_create_another_admin(self):
        """
            TC1.

            Initial State:
                setUp
            Action:
                Attempt to create another admin
            Expected Result:
                Notification: admin is already created
        """

        User.objects.create(username='cool hacker', is_superuser=True)
        self.assertTrue(len(User.objects.filter(is_superuser=True)) == 1)

    def test_admin_creates_librarians(self):
        """
            TC2.

            Initial State:
                setUp
            Action:
                i. admin1 creates l1
                ii. admin1 creates l2
                iii. admin1 creates l3
            Expected Result:
                System has 3 new librarians
        """
        # region Action
        self.l1 = User.objects.create(username="l1", password="12345")
        self.l2 = User.objects.create(username="l2", password="12345")
        self.l3 = User.objects.create(username="l3", password="12345")

        self.l1.groups.add(Group.objects.get(name="Access Level 1"))
        self.l2.groups.add(Group.objects.get(name="Access Level 2"))
        self.l3.groups.add(Group.objects.get(name="Access Level 3"))

        group_librarian = Group.objects.get(name="librarian")

        self.l1.groups.add(group_librarian)
        self.l2.groups.add(group_librarian)
        self.l3.groups.add(group_librarian)

        # endregion

        self.assertTrue(len(User.objects.filter(groups__name='librarian')) == 3)

    def test_librarian_has_no_rights_to_create_documents(self):
        """
        TC3.

            Initial State:
                Run TC2
            Action:
                i.  l1 creates d1(3), d2(3), d3(3)
                ii. l1 checks the information or the system

            Expected Result:
                Notification: l1 has no rights to create documents
        """
        self.test_admin_creates_librarians()
        for permission in self.l1.get_all_permissions():
            self.assertFalse('add' in permission)

    def test_librarian_creates_documents_patrons(self):
        """
        TC4.

            Initial State:
                Run TC2
            Action:
                i.      l2 creates d1(3), d2(3), d3(3)
                ii.     l2 creates patrons s,p1,p2,p3,v
                iii.    l2 checks the information of the system

            Expected Result:
                All documents and patrons are successfully created
        """
        self.test_admin_creates_librarians()

        # region Action

        # region Document Creation
        authors = []
        docs = []
        # create d1 - docs[0]
        authors.append(Author.objects.create(
            first_name="Thomas", last_name="H. Cormen"))
        authors.append(Author.objects.create(
            first_name="Charles", last_name="E. Leiserson"))
        authors.append(Author.objects.create(
            first_name="Ronald", last_name="L. Rivest"))
        authors.append(Author.objects.create(
            first_name="Clifford", last_name="Stein"))

        tags = []

        tags.append(Tag.objects.create(title='Algorithms'))
        tags.append(Tag.objects.create(title='Data Structures'))
        tags.append(Tag.objects.create(title='Complexity'))
        tags.append(Tag.objects.create(title='Computational Theory'))

        p = Publisher.objects.create(
            title="MIT Press", created_date=datetime.datetime.now(tz=timezone.utc))

        self.d1 = Book.objects.create(title="Introduction to Algorithms",
                                      published_date=datetime.datetime(
                                          year=2009, month=1, day=1, tzinfo=pytz.UTC),
                                      publisher=p,
                                      issue=3, copies=3, price=5000)

        docs.append(self.d1)

        docs[0].authors.set(authors)
        self.d1.tags.set(tags)
        authors.clear()
        tags.clear()

        # create d2 - docs[1]

        authors.append(Author.objects.create(
            first_name="Niklaus", last_name="Wirth"))

        p = Publisher.objects.create(title="Prentice Hall PTR")

        self.d2 = Book.objects.create(title="Algorithms + Data Structures = Programs",
                                      is_bestseller=False,
                                      published_date=datetime.datetime(
                                          year=1978, month=1, day=1, tzinfo=pytz.UTC),
                                      publisher=p,
                                      issue=1, copies=3, price=5000)

        tags.append(Tag.objects.get(title='Algorithms'))
        tags.append(Tag.objects.get(title='Data Structures'))
        tags.append(Tag.objects.create(title='Search Algorithms'))
        tags.append(Tag.objects.create(title='Pascal'))

        docs.append(self.d2)
        self.d2.tags.set(tags)
        docs[1].authors.set(authors)

        authors.clear()
        tags.clear()

        # create d3 - docs[2]

        authors.append(Author.objects.create(
            first_name="Donald", last_name="E. Knuth"))

        p = Publisher.objects.create(title="Addison Wesley Longman Publishing Co., Inc.")

        self.d3 = Book.objects.create(title="The Art of Computer Programming",
                                      is_bestseller=False,
                                      published_date=datetime.datetime(
                                          year=1978, month=1, day=1, tzinfo=pytz.UTC),
                                      publisher=p,
                                      issue=3, copies=3, price=5000)

        tags.append(Tag.objects.get(title='Algorithms'))
        tags.append(Tag.objects.create(title='Combinatorial Algorithms'))
        tags.append(Tag.objects.create(title='Recursion'))

        docs.append(self.d3)
        self.d3.tags.set(tags)
        docs[2].authors.set(authors)

        authors.clear()
        tags.clear()

        # endregion Books Creation

        # region Patron Creation
        # create p1 - patron
        self.p1 = User.objects.create(
            username="p1", first_name="Sergey", last_name="Afonso", password="12345", email='root@root.ru')
        self.p1.groups.add(Group.objects.get(name="faculty"))
        self.p1.profile.address = "Via Margutta, 3"
        self.p1.profile.phone_number = "30001"
        self.p1.profile.type = 5
        self.p1.save()

        # create p2 - patron
        self.p2 = User.objects.create(
            username="p2", first_name="Nadia", last_name="Teixeira", password="12345", email='root@root.ru')
        self.p2.groups.add(Group.objects.get(name="faculty"))
        self.p2.profile.address = "Via Sacra, 13"
        self.p2.profile.phone_number = "30002"
        self.p2.profile.type = 5
        self.p2.save()

        # create p3 - patron
        self.p3 = User.objects.create(
            username="p3", first_name="Elvira", last_name="Espindola", password="12345", email='root@root.ru')
        self.p3.groups.add(Group.objects.get(name="faculty"))
        self.p3.profile.address = "Via del Corso, 22"
        self.p3.profile.phone_number = "30003"
        self.p3.profile.type = 5
        self.p3.save()

        # create s - patron
        self.s = User.objects.create(
            username="s", first_name="Andrey", last_name="Velo", password="12345", email='root@root.ru')
        self.s.groups.add(Group.objects.get(name="student"))
        self.s.profile.address = "Avenida Mazatlan, 250"
        self.s.profile.phone_number = "30004"
        self.s.profile.type = 1
        self.s.save()

        # create v - patron
        self.v = User.objects.create(
            username="v", first_name="Veronika", last_name="Rama", password="12345", email='root@root.ru')
        self.v.groups.add(Group.objects.get(name="visiting professor"))
        self.v.profile.address = "Stret Atocha, 27"
        self.v.profile.phone_number = "30005"
        self.v.profile.type = 4
        self.v.save()
        self.assertTrue(documents_copies_number(Document.objects.all()) == 9 and
                        len(User.objects.all()) == 9)
        # endregion

        # endregion Action

    def test_librarian_deletes_document(self):
        """
        TC5.

        Initial State:
            Run TC4
        Action:
            i.  l3 deletes d1(1)
            ii. l2 checks the information or the system

        Expected Result:
            The system has only 2 copies of d2
        """
        self.test_librarian_creates_documents_patrons()

        # region Action
        self.d1.copies = self.d1.copies - 1
        self.d1.save()
        # endregion Action

        self.assertTrue(self.d1.copies == 2)

    def test_librarian_has_no_rights_to_place_outstanding_requests(self):
        """
        TC6.

        Initial State:
            Run TC4
        Action:
            i.  p1 checks out d3
            ii. p2 checks out d3
            iii. s checks out d3
            iv. v checks out d3
            v. p3 checks out d3
            vi. l1 places an outstanding request on document d3

        Expected Result:
            Notification: l1 has no rights to place outstanding requests
        """
        self.test_librarian_creates_documents_patrons()

        # region Action
        self.p1.profile.checkout_document(self.d3)
        self.p1.profile.checkout_document(self.d3)
        self.p1_d3 = Record.objects.get(user=self.p1, document=self.d3)
        self.p1_d3.status = Record.TAKEN
        self.p1_d3.save()

        self.p2.profile.checkout_document(self.d3)
        self.p2_d3 = Record.objects.get(user=self.p2, document=self.d3)
        self.p2_d3.status = Record.TAKEN
        self.p2_d3.save()

        self.s.profile.checkout_document(self.d3)
        self.s_d3 = Record.objects.get(user=self.s, document=self.d3)
        self.s_d3.status = Record.TAKEN
        self.s_d3.save()

        self.v.profile.checkout_document(self.d3)
        self.v_d3 = Record.objects.get(user=self.v, document=self.d3)

        self.p3.profile.checkout_document(self.d3)
        self.p3_d3 = Record.objects.get(user=self.p3, document=self.d3)

        # endregion Action

        self.assertFalse(self.l1.has_perm('website.add_request'))

    def test_librarian_has_rights_to_place_outstanding_requests(self):
        """
        TC7.

        Initial State:
            Run TC4
        Action:
            i.  p1 checks out d3
            ii. p2 checks out d3
            iii. s checks out d3
            iv. v checks out d3
            v. p3 checks out d3
            vi. l3 places an outstanding request on document d3

        Expected Result:
            i.   waiting list for document d3 is empty
            ii.  p1 and p2 are notified to return the respective books
            iii. s, v and p3 are notified that document d3 is not longer
                available and that they have been removed from the waiting list
        """
        self.test_librarian_creates_documents_patrons()

        # region Action
        self.p1.profile.checkout_document(self.d3)
        self.p1_d3 = Record.objects.get(user=self.p1, document=self.d3)
        self.p1_d3.status = Record.TAKEN
        self.p1_d3.save()

        self.p2.profile.checkout_document(self.d3)
        self.p2_d3 = Record.objects.get(user=self.p2, document=self.d3)
        self.p2_d3.status = Record.TAKEN
        self.p2_d3.save()

        self.s.profile.checkout_document(self.d3)
        self.s_d3 = Record.objects.get(user=self.s, document=self.d3)
        self.s_d3.status = Record.TAKEN
        self.s_d3.save()

        self.v.profile.checkout_document(self.d3)
        self.v_d3 = Record.objects.get(user=self.v, document=self.d3)

        self.p3.profile.checkout_document(self.d3)
        self.p3_d3 = Record.objects.get(user=self.p3, document=self.d3)

        Request.objects.create(
            user=self.l3, document=self.d3, type=Request.OUTSTANDING_REQUEST)

        self.assertFalse(get_waiting_list(Record.objects.filter(document=self.d3)))

        for request in Request.objects.filter(type=Request.DEFERRED_BOOK):
            self.assertTrue(request.is_sent)
        for request in Request.objects.filter(type=Request.RETURN_BOOK):
            self.assertTrue(request.is_sent)
        # endregion Action

        self.assertEqual(len(mail.outbox), 5)

    def test_admin_checks_log_before_outstanding_request(self):
        """
        TC 8.

        Initial State:
            Run TC 6
        Action:
            Admin checks logs
        Expected Result:
            i. date: admin1 created librarian l1
            ii. date: admin1 created librarian l2
            iii. date: admin1 created librarian l3
            iv. date: l2 created 3 copies of d1
            v. date: l2 created 3 copies of d2
            vi. date: l2 created 3 copies of d3
            vii. date: l2 created patrons s, p1, p2, p3 and v
            viii. date: l2 checked the System’s information.
            ix. p1 checked out d3
            x. p2 checked out d3
            xi. s checked out d3
            xii. v checked out d3
            xiii. p3 checked out d3
            xiv. l1 placed an outstanding request on document d3 : request was denied.
        """
        self.test_librarian_has_no_rights_to_place_outstanding_requests()
        self.assertTrue(len(User.objects.filter(groups__name='librarian')) == 3)
        self.assertTrue(len(Document.objects.all()) == 3)

        self.assertTrue(LogEntry.objects.filter(object_id=self.p1_d3.id, user=self.p1).exists())
        self.assertTrue(LogEntry.objects.filter(object_id=self.p2_d3.id, user=self.p2).exists())
        self.assertTrue(LogEntry.objects.filter(object_id=self.s_d3.id, user=self.s).exists())
        self.assertTrue(LogEntry.objects.filter(object_id=self.v_d3.id, user=self.v).exists())
        self.assertTrue(LogEntry.objects.filter(object_id=self.p3_d3.id, user=self.p3).exists())

    def test_admin_checks_log_after_outstanding_request(self):
        """
        TC 9.

        Initial State:
            Run TC 7
        Action:
            Admin checks logs
        Expected Result:
            i. date: admin1 created librarian l1
            ii. date: admin1 created librarian l2
            iii. date: admin1 created librarian l3
            iv. date: l2 created 3 copies of d1
            v. date: l2 created 3 copies of d2
            vi. date: l2 created 3 copies of d3
            vii. date: l2 created patrons s, p1, p2, p3 and v
            viii. date: l2 checked the System’s information.
            ix. p1 checked out d3
            x. p2 checked out d3
            xi. s checked out d3
            xii. v checked out d3
            xiii. p3 checked out d3
            xiv. l3 placed an outstanding request on document d3
            xv.  Waiting list for document d3 was deleted.
            xvi. p1, p1 and p2 were notified to return the respective books.
            xvii. v and p3 were notified that document d3 is not longer available and that they have been removed from the waiting list.
            """
        self.test_librarian_has_rights_to_place_outstanding_requests()
        self.assertTrue(len(User.objects.filter(groups__name='librarian')) == 3)
        self.assertTrue(len(Document.objects.all()) == 3)

        self.assertTrue(LogEntry.objects.filter(object_id=self.p1_d3.id, user=self.p1).exists())
        self.assertTrue(LogEntry.objects.filter(object_id=self.p2_d3.id, user=self.p2).exists())
        self.assertTrue(LogEntry.objects.filter(object_id=self.s_d3.id, user=self.s).exists())
        self.assertTrue(LogEntry.objects.filter(object_id=self.v_d3.id, user=self.v).exists())
        self.assertTrue(LogEntry.objects.filter(object_id=self.p3_d3.id, user=self.p3).exists())

        self.assertFalse(get_waiting_list(Record.objects.filter(document=self.d3)))

        for request in Request.objects.filter(type=Request.DEFERRED_BOOK):
            self.assertTrue(request.is_sent)
        for request in Request.objects.filter(type=Request.RETURN_BOOK):
            self.assertTrue(request.is_sent)

    def test_patron_tries_to_search_for_book_by_full_title(self):
        """
        T10.

        Initial State:
            Run TC4
        Action:
            Patron v searches for a book by title: Introduction to Algorithms
        Expected Result:
            Introduction to Algorithms by Cormen et al.
        """
        self.test_librarian_creates_documents_patrons()
        self.assertEqual(Document.objects.get(pk=self.d1.id), search('Introduction to Algorithms').first())

    def test_patron_tries_to_search_for_book_by_partial_title(self):
        """
        T11.

        Initial State:
            Run TC4
        Action:
            Patron v searches for a book by title: Algorithms
        Expected Result:
            Introduction to Algorithms by Cormen et al.
            Algorithms + Data Structures = Programs by Niklaus Wirth
        """
        self.test_librarian_creates_documents_patrons()
        search_result = search('Algorithms')
        self.assertEqual(Document.objects.get(pk=self.d1.id), search_result[0])
        self.assertEqual(Document.objects.get(pk=self.d2.id), search_result[1])

    def test_patron_tries_to_search_for_book_by_tag(self):
        """
        T12.

        Initial State:
            Run TC4
        Action:
            Patron v searches for a book by keywords: Algorithms
        Expected Result:
            Introduction to Algorithms by Cormen et al.
            Algorithms + Data Structures = Programs by Niklaus Wirth
            The Art of Computer Programming by Donald E. Knuth
        """
        self.test_librarian_creates_documents_patrons()
        self.assertTrue(search_helper(None, 'Algo'))

    def test_patron_tries_to_search_for_book_with_AND(self):
        """
        T13.

        Initial State:
            Run TC4
        Action:
            Patron v searches for a book by title Algorithms AND Programming
        Expected Result:
            No search result
        """
        self.test_librarian_creates_documents_patrons()
        search_result = search_helper('Algorithms AND Programming')
        self.assertFalse(search_result)

    def test_patron_tries_to_search_for_book_with_OR(self):
        """
        T14.

        Initial State:
            Run TC4
        Action:
            Patron v searches for a book by title Algorithms OR Programming
        Expected Result:
            Introduction to Algorithms by Cormen et al.
            Algorithms + Data Structures = Programs by Niklaus Wirth
            The Art of Computer Programming by Donald E. Knuth
        """
        self.test_librarian_creates_documents_patrons()
        search_result = search_helper('Algo OR Prog')
        self.assertEqual(len(search_result), 3)
