from django.contrib.auth.models import User, Group
from django.test import TestCase
from django.utils import timezone
import datetime

from ..models.author import Author
from ..models.publisher import Publisher
from ..models.book import Book
from ..models.document import Document
from ..models.record import Record

"""
All tests should be in package named (module_name)/tests/...
Django will automatically start every test which starts with test*.py
And execute every test which starts with "test"

To start your tests you need to write
"./manage.py test *name_of_testings*" in your docker's bash
(To enter docker bash write in main directory "docker exec -it lms_python_1 bash")
Sometimes you need to explicitly write
$  python3 ./manage.py test

For some reason django couldn't find tests in website so you need to write
$  manage.py test website.tests

To test something specifically write
$  ./manage.py test website.tests.*class_name*.*test_name*


You can specify a custom filename pattern match using the -p (or --pattern) option,
if your test files are named differently from the test*.py pattern:

$ ./manage.py test --pattern="tests_*.py"

You can prevent the test databases from being destroyed by using the "test --keepdb" option.

As long as your tests are properly isolated,
you can run them in parallel to gain a speed up on multi-core hardware. Use test --parallel.

Functions used
setUpTestData: Run once to set up non-modified data for all class methods.
setUp:  Run once for every test method to setup clean data.
tearDown:  Clean up run after every test method.

All tests should be named this way:
[test_MethodName_StateUnderTest_ExpectedBehavior]
If you want your test to run with other you MUST write "test" at the beginning of the your test


The idea of naming them like this was took from Roy Osherove
Source: http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
"""


class TestSuiteDelivery1(TestCase):
    """This module has unit tests specified in test cases for first delivery"""

    def setUp(self):
        number_of_items = 2
        Author.objects.create(first_name="David", last_name="Patterson",
                              created_date=datetime.datetime.now(tz=timezone.utc))

        Publisher.objects.create(
            title="MIT", created_date=datetime.datetime.now(tz=timezone.utc))

        Book.objects.create(title="Computer Architecture", created_date=datetime.datetime.now(tz=timezone.utc),
                            description="Sample description",
                            published_date=datetime.datetime.now(
                                tz=timezone.utc),
                            publisher=Publisher.objects.all()[0],
                            issue=1, copies=number_of_items)
        Document.objects.all()[0].authors.set([Author.objects.all()[0]])

        # Define some group names
        self.faculty_group = Group.objects.get(name="faculty")
        self.faculty_group.save()
        self.librarian_group = Group.objects.get(name="librarian")
        self.librarian_group.save()
        self.student_group = Group.objects.get(name="student")
        self.student_group.save()

        User.objects.create_user(username='testuser1', password='12345')
        User.objects.create_user(username='testuser2', password='12345')
        User.objects.create_user(username='testuser3', password='12345')
        User.objects.create_user(username='testuser4', password='12345')

        User.objects.create_user(username='librarian1', password='12345')

    def test_CheckOut_GeneralCase_BookIsCheckedOut(self):

        print("\nTest CheckOut_GeneralCase_BookIsCheckedOut begins")
        patron = User.objects.get(username="testuser1")
        patron.groups.add(self.student_group)
        patron.save()

        librarian = User.objects.get(username="testuser2")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        document = Document.objects.all()[0]
        document.copies = 2
        patron.profile.checkout_document(document)

        record = Record.objects.filter(user=patron)

        quantity_is_equal = document.copies - len(record) == 1
        self.assertTrue((quantity_is_equal and (
                len(record) == 1) and (record[0] is not None)))

        print("Test CheckOut_GeneralCase_BookIsCheckedOut is done successfully")

        # Effect:Librarians can see that Patron p has one copy of book b and there is one copy of book b in the library.

    def test_CheckOut_AuthorDoesNotExist_NothingChanges(self):

        print("\nTest CheckOut_AuthorDoesNotExist_NothingChanges begins")

        patron = User.objects.get(username="testuser1")
        patron.groups.add(self.student_group)
        patron.save()

        librarian = User.objects.get(username="testuser2")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        some_author = Author.objects.create(first_name="Daud", last_name="Patterson",
                                            created_date=datetime.datetime.now(tz=timezone.utc))
        some_author.save()
        doc = Document.objects.filter(authors=some_author)

        try:
            doc.checkout_document(patron)
            checked_out = True
        except AttributeError:
            checked_out = False
        else:
            checked_out = False
        self.assertFalse(checked_out)
        print("Test CheckOut_AuthorDoesNotExist_NothingChanges is done successfully")

        """Effect: The system does not change its state.
        Maybe a message notifying the patron can read: the  library does not have book 'b'"""

    def test_CheckOutTime_FacultyChecksGeneralBook_FourWeeksToReturn(self):

        print("\nTest CheckOutTime_FacultyChecksGeneralBook_FourWeeksToReturn begins")

        faculty = User.objects.get(username="testuser1")
        faculty.groups.add(self.faculty_group)
        faculty.save()

        student = User.objects.get(username="testuser2")
        student.groups.add(self.student_group)
        student.save()

        librarian = User.objects.get(username="librarian1")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        document = Document.objects.all()[0]
        faculty.profile.checkout_document(document)

        record = Record.objects.get(document=document, user=faculty)
        return_date = record.get_return_date()
        self.assertEqual((record.created_date +
                          datetime.timedelta(days=28)).date(), return_date.date())

        print("Test CheckOutTime_FacultyChecksGeneralBook_FourWeeksToReturn is done successfully")

        # Effect:The book is checked out by 'f' with returning time of 4 weeks (from the day it was checked out)

    def test_CheckOutTime_FacultyChecksBestseller_FourWeeksToReturn(self):

        print("\nTest CheckOutTime_FacultyChecksBestseller_FourWeeksToReturn begins")

        faculty = User.objects.get(username="testuser1")
        faculty.groups.add(self.faculty_group)
        faculty.save()

        student = User.objects.get(username="testuser2")
        student.groups.add(self.student_group)
        student.save()

        librarian = User.objects.get(username="librarian1")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        document = Document.objects.all()[0]
        document.is_bestseller = True
        faculty.profile.checkout_document(document)

        record = Record.objects.get(document=document, user=faculty)

        return_date = record.get_return_date()
        self.assertEqual((record.created_date +
                          datetime.timedelta(days=28)).date(), return_date.date())

        print("Test CheckOutTime_FacultyChecksBestseller_FourWeeksToReturn is done successfully")

        # Effect: The book is checked out by 'f' with returning time of 4 weeks (from the day it was checked out)

    def test_CheckOutCopies_ThreePatronsChecksOneBook_OnlyTwoSucceeds(self):
        print("\nTest CheckOutCopies_ThreePatronsChecksOneBook_OnlyTwoSucceeds begins")

        student1 = User.objects.get(username="testuser1")
        student1.groups.add(self.student_group)
        student1.save()

        student2 = User.objects.get(username="testuser2")
        student2.groups.add(self.student_group)
        student2.save()

        student3 = User.objects.get(username="testuser3")
        student3.groups.add(self.student_group)
        student3.save()

        librarian = User.objects.get(username="librarian1")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        document = Document.objects.all()[0]
        document.copies = 2

        record1 = student1.profile.checkout_document(document)
        record1.status = Record.TAKEN
        record1.save()

        record2 = student2.profile.checkout_document(document)
        record2.status = Record.TAKEN
        record2.save()

        # setting statuses
        record3 = student3.profile.checkout_document(document)
        record3.status = Record.WAITING
        record3.save()

        self.assertTrue(record1.status == "TK")
        self.assertTrue(record2.status == "TK")
        self.assertTrue(record3.status == "WG")
        print("Test CheckOutCopies_ThreePatronsChecksOneBook_OnlyTwoSucceeds is done successfully")

        # Effect:Only first two patrons can check out the copy of book A. The third patron sees an empty list of books.

    def test_CheckOut_PatronsTriesToCheckOutSameBookTwice_NoChanges(self):
        print("\nTest CheckOut_PatronsTriesToCheckOutSameBookTwice_NoChanges begins")

        student1 = User.objects.get(username="testuser1")
        student1.groups.add(self.student_group)
        student1.save()

        librarian = User.objects.get(username="librarian1")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        document = Document.objects.all()[0]

        student1.profile.checkout_document(document)
        student1.profile.checkout_document(document)

        self.assertTrue(len(Record.objects.filter(user=student1)) == 1)
        print("Test CheckOut_PatronsTriesToCheckOutSameBookTwice_NoChanges is done successfully")

        """Effect:None. In particular, librarians can check that Patron p
        has the same copy c of book b as beforeand copy c' is still in the library """

    def test_Records_TwoPatronsCheckOutTwoBooks_CanTrackBoth(self):
        print("\nTest Records_TwoPatronsCheckOutTwoBooks_CanTrackBoth begins")

        patron1 = User.objects.get(username="testuser1")
        patron1.groups.add(self.student_group)
        patron1.save()

        patron2 = User.objects.get(username="testuser2")
        patron2.groups.add(self.student_group)
        patron2.save()

        librarian = User.objects.get(username="librarian1")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        document = Document.objects.all()[0]
        patron1.profile.checkout_document(document)
        patron2.profile.checkout_document(document)

        record1 = Record.objects.get(user=patron1)
        record2 = Record.objects.get(user=patron2)

        self.assertTrue(record1 is not None and record2 is not None)
        print("Test Records_TwoPatronsCheckOutTwoBooks_CanTrackBoth is done successfully")

        # Effect: The system should track both bookings

    def test_CheckOutTime_StudentChecksOutGeneralBook_ThreeWeeksToReturn(self):
        print("\nTest CheckOutTime_StudentChecksOutGeneralBook_ThreeWeeksToReturn begins")

        faculty = User.objects.get(username="testuser1")
        faculty.groups.add(self.faculty_group)
        faculty.save()

        student = User.objects.get(username="testuser2")
        student.groups.add(self.student_group)
        student.save()

        librarian = User.objects.get(username="librarian1")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        document = Document.objects.all()[0]
        student.profile.checkout_document(document)

        record = Record.objects.get(document=document, user=student)

        return_date = record.get_return_date()
        self.assertEqual((record.created_date +
                          datetime.timedelta(days=21)).date(), return_date.date())
        print("Test CheckOutTime_StudentChecksOutGeneralBook_ThreeWeeksToReturn is done successfully")

        # Effect -  The book is checked out by 's' with returning time of 3 weeks (from the day it was checked out)

    def test_CheckOutTime_StudentChecksOutBestseller_TwoWeeksToReturn(self):
        print("\nTest CheckOutTime_StudentChecksOutBestseller_TwoWeeksToReturn begins")

        faculty = User.objects.get(username="testuser1")
        faculty.groups.add(self.faculty_group)
        faculty.save()

        student = User.objects.get(username="testuser2")
        student.groups.add(self.student_group)
        student.save()

        librarian = User.objects.get(username="librarian1")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        document = Book.objects.create(title="Penny Wise", created_date=datetime.datetime.now(tz=timezone.utc),
                                       description="Waiting for you",
                                       published_date=datetime.datetime.now(
                                           tz=timezone.utc),
                                       publisher=Publisher.objects.all()[0],
                                       issue=1, copies=1, is_bestseller=True)

        student.profile.checkout_document(document)

        record = Record.objects.get(document=document, user=student)

        d1 = record.created_date + datetime.timedelta(days=14)
        d2 = record.get_return_date()

        self.assertEquals(d1.date(), d2.date())
        print("Test CheckOutTime_StudentChecksOutBestseller_TwoWeeksToReturn is done successfully")

        # Effect - The book is checked out by 's' with returning time of 2 weeks (from the day it was checked out)

    def test_CheckOut_PatronChecksOutReferenceBookAndGeneralBook_ChecksOutOnlyOne(self):
        print("\nTest CheckOut_PatronChecksOutReferenceBookAndGeneralBook_ChecksOutOnlyOne begins")

        number_of_items = 2
        student1 = User.objects.get(username="testuser1")
        student1.groups.add(self.student_group)
        student1.save()

        librarian = User.objects.get(username="librarian1")
        librarian.groups.add(self.librarian_group)
        librarian.save()

        document1 = Document.objects.all()[0]
        document2 = Document.objects.create(title="Computer Architecture2",
                                            created_date=datetime.datetime.now(
                                                tz=timezone.utc),
                                            description="Sample description",
                                            published_date=datetime.datetime.now(
                                                tz=timezone.utc),
                                            copies=number_of_items,
                                            is_reference=True)
        document2.authors.set([Author.objects.all()[0]])
        document2.save()

        self.assertTrue(student1.profile.checkout_document(document1))
        self.assertFalse(student1.profile.checkout_document(document2))
        print("Test CheckOut_PatronChecksOutReferenceBookAndGeneralBook_ChecksOutOnlyOne is done successfully")

        # Effect:The system allows to check out only the book A. The reference book B is not available for checking out
