import pytz
from django.contrib.auth.models import User, Group
from django.test import TestCase
from django.utils import timezone
import datetime

from ..models.author import Author
from ..models.publisher import Publisher
from ..models.book import Book
from ..models.av import AV
from ..models.document import Document
from ..models.record import Record
from ..models.request import Request

from ..utils import documents_copies_number, get_overdued_records, get_waiting_list

"""
All tests should be in package named (module_name)/tests/...
Django will automatically start every test which starts with test*.py
And execute every test which starts with "test"

To start your tests you need to write
"./manage.py test *name_of_testings*" in your docker's bash
(To enter docker bash write in main directory "docker exec -it lms_python_1 bash")
Sometimes you need to explicitly write
$  python3 ./manage.py test

For some reason django couldn't find tests in website so you need to write
$  manage.py test website.tests

To test something specifically write
$  ./manage.py test website.tests.*class_name*.*test_name*


You can specify a custom filename pattern match using the -p (or --pattern) option,
if your test files are named differently from the test*.py pattern:

$ ./manage.py test --pattern="tests_*.py"

You can prevent the test databases from being destroyed by using the "test --keepdb" option.

As long as your tests are properly isolated,
you can run them in parallel to gain a speed up on multi-core hardware. Use test --parallel.

Functions used
setUpTestData: Run once to set up non-modified data for all class methods.
setUp:  Run once for every test method to setup clean data.
tearDown:  Clean up run after every test method.

All tests should be named this way:
[test_MethodName_StateUnderTest_ExpectedBehavior]
If you want your test to run with other you MUST write "test" at the beginning of the your test


The idea of naming them like this was took from Roy Osherove
Source: http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html


Each test should be described in format:
Test Case 1
    Initial state:      p1 checked out d1 and d2 on March 5th
    Action:             I.  p1 returns d2
                        II. librarian checks the dues and fines of p1
    Expected Result:    Overdue: [(d1, 0 days]
                        Fine: [(0 days, 0 rub)]


"""


class TestSuiteDelivery3(TestCase):
    """This module has unit tests specified in test cases for delivery #3"""

    def setUp(self):
        """This test creates 3 copies of book b1, 3 of b2, 2 of b3, patrons p1,p2,p3, librarian"""
        # region Books Creation
        authors = []
        docs = []
        self.today = datetime.datetime(
            year=2018, month=4, day=2, tzinfo=pytz.UTC)
        # create d1 - docs[0]
        authors.append(Author.objects.create(
            first_name="Thomas", last_name="H. Cormen"))
        authors.append(Author.objects.create(
            first_name="Charles", last_name="E. Leiserson"))
        authors.append(Author.objects.create(
            first_name="Ronald", last_name="L. Rivest"))
        authors.append(Author.objects.create(
            first_name="Clifford", last_name="Stein"))

        p = Publisher.objects.create(
            title="MIT Press", created_date=datetime.datetime.now(tz=timezone.utc))

        self.d1 = Book.objects.create(title="Introduction to Algorithms",
                                      published_date=datetime.datetime(
                                          year=2009, month=1, day=1, tzinfo=pytz.UTC),
                                      publisher=p,
                                      issue=3, copies=3, price=5000)

        docs.append(self.d1)

        docs[0].authors.set(authors)

        authors.clear()

        # create d2 - docs[1]

        authors.append(Author.objects.create(
            first_name="Erich", last_name="Gamma"))
        authors.append(Author.objects.create(
            first_name="Ralph", last_name="Johnson"))
        authors.append(Author.objects.create(
            first_name="John", last_name="Vlissides"))
        authors.append(Author.objects.create(
            first_name="Richard", last_name="Helm"))

        p = Publisher.objects.create(title="Addison-Wesley Professional")

        self.d2 = Book.objects.create(title="Design Patterns: Elements of Reusable Object-Oriented Software",
                                      is_bestseller=True,
                                      published_date=datetime.datetime(
                                          year=2003, month=1, day=1, tzinfo=pytz.UTC),
                                      publisher=p,
                                      issue=1, copies=3, price=1700)
        docs.append(self.d2)
        docs[1].authors.set(authors)

        authors.clear()

        # create d3 - docs[2]

        authors.append(Author.objects.create(
            first_name="Tony", last_name="Hoare"))

        p = Publisher.objects.create(
            title="Addison-Wesley Longman Publishing Co., Inc.")

        self.d3 = AV.objects.create(title="Null References: The Billion Dollar Mistake",
                                    copies=2, price=700)
        docs.append(self.d3)
        docs[2].authors.set(authors)
        authors.clear()

        # endregion Books Creation

        # region Patrons Creation

        # create p1 - patron
        self.p1 = User.objects.create(
            username="p1", first_name="Sergey", last_name="Afonso", password="12345")
        self.p1.groups.add(Group.objects.get(name="faculty"))
        self.p1.profile.address = "Via Margutta, 3"
        self.p1.profile.phone_number = "30001"
        self.p1.save()

        # create p2 - patron
        self.p2 = User.objects.create(
            username="p2", first_name="Nadia", last_name="Teixeira", password="12345")
        self.p2.groups.add(Group.objects.get(name="faculty"))
        self.p2.profile.address = "Via Sacra, 13"
        self.p2.profile.phone_number = "30002"
        self.p2.save()

        # create p3 - patron
        self.p3 = User.objects.create(
            username="p3", first_name="Elvira", last_name="Espindola", password="12345")
        self.p3.groups.add(Group.objects.get(name="faculty"))
        self.p3.profile.address = "Via del Corso, 22"
        self.p3.profile.phone_number = "30003"
        self.p3.save()

        # create s - patron
        self.s = User.objects.create(
            username="s", first_name="Andrey", last_name="Velo", password="12345")
        self.s.groups.add(Group.objects.get(name="student"))
        self.s.profile.address = "Avenida Mazatlan, 250"
        self.s.profile.phone_number = "30004"
        self.s.save()

        # create v - patron
        self.v = User.objects.create(
            username="v", first_name="Veronika", last_name="Rama", password="12345")
        self.v.groups.add(Group.objects.get(name="visiting professor"))
        self.v.profile.address = "Stret Atocha, 27"
        self.v.profile.phone_number = "30005"
        self.v.save()

        # create librarian
        self.librarian = User.objects.create(username="l", password="12345")
        self.librarian.groups.add(Group.objects.get(name="librarian"))

        # endregion Patrons Creation

        self.assertTrue(documents_copies_number(Document.objects.all()) == 8 and
                        len(User.objects.all()) == 6)

    def test_patronCheckedOutBooks_patronReturnsLibrarianChecks(self):
        """ Test Case 1
            Initial state:      p1 checked out d1 and d2 on March 5th
            Action:             I.  p1 returns d2
                                II. librarian checks the dues and fines of p1
            Expected Result:    Overdue: [(d1, 0 days]
                                Fine: [(0 days, 0 rub)]
        """
        # region Initial State
        t = self.p1.profile.checkout_document(self.d1)
        t.created_date = datetime.datetime(year=2018, month=3, day=5, tzinfo=pytz.UTC)
        t = self.p1.profile.checkout_document(self.d2)
        t.created_date = datetime.datetime(year=2018, month=3, day=5, tzinfo=pytz.UTC)

        # endregion Initial State

        # region Action
        self.p1.profile.return_document(self.d2)

        records = get_overdued_records(
            Record.objects.all().filter(user=self.p1), self.today)
        self.assertTrue(len(records) == 0)
        p1_d1 = Record.objects.all().get(user=self.p1, document=self.d1)
        self.assertTrue(p1_d1.get_overdue_days(self.today) == 0)
        self.assertTrue(p1_d1.get_fine(self.today) == 0)

        # endregion Action

    def test_UsersCheckOutDocumentsLibrarianChecksDue_DueDateAndFineIsCorrect(self):
        """ Test Case 2
            Initial State:      p1 checked out d1 and d2 on March 5th.
                                s checked out d1 and d2 on March 5th.
                                v checked out d1 and d2 on March 5th
            Action:             The librarian checks the dues and fines (if any) of the library’s assets
            Expected Result:    Assuming  that  the  action  happened  today  (April 2nd),the information should be
                                p1 Overdue:[(d1, 0 days), (d2, 0 days)] Fine:[(0 days, 0 rub.), (0 days, 0 rub.)]
                                s Overdue:[(d1, 7 days), (d2, 14 days)] Fine:[(7 days, 700 rub.), (14 days, 1400 rub.)]
                                v Overdue:[(d1, 21 days), (d2, 21 days)] Fine:[(21 days, 2100 rub.), (21 days, 1700 rub.)]
        """
        print(
            "Test UsersCheckOutDocumentsLibrarianChecksDue_DueDateAndFineIsCorrect begins")

        p1 = User.objects.all().get(username="p1")
        v = User.objects.all().get(username="v")
        s = User.objects.all().get(username="s")
        d1 = Book.objects.all().get(title="Introduction to Algorithms")
        d2 = Book.objects.all().get(
            title="Design Patterns: Elements of Reusable Object-Oriented Software")
        march_5 = datetime.datetime(
            year=2018, month=3, day=5, tzinfo=pytz.UTC)

        t = p1.profile.checkout_document(d1)
        t.created_date = march_5
        t.save()

        t = p1.profile.checkout_document(d2)
        t.created_date = march_5
        t.save()

        t = s.profile.checkout_document(d1)
        t.created_date = march_5
        t.save()

        t = s.profile.checkout_document(d2)
        t.created_date = march_5
        t.save()

        t = v.profile.checkout_document(d1)
        t.created_date = march_5
        t.save()

        t = v.profile.checkout_document(d2)
        t.created_date = march_5
        t.save()

        # endregion Initial State

        records_p1 = Record.objects.all().filter(user=p1)
        p1_d1 = records_p1.get(document=d1)
        p1_d1.last_status_modified_date = march_5
        p1_d1.save()

        p1_d2 = records_p1.get(document=d2)
        p1_d2.last_status_modified_date = march_5
        p1_d2.save()

        records_v = Record.objects.all().filter(user=v)
        v_d1 = records_v.get(document=d1)
        v_d1.last_status_modified_date = march_5
        v_d1.save()

        v_d2 = records_v.get(document=d2)
        v_d2.last_status_modified_date = march_5
        v_d2.save()

        records_s = Record.objects.all().filter(user=s)
        s_d1 = records_s.get(document=d1)
        s_d1.last_status_modified_date = march_5
        s_d1.save()

        s_d2 = records_s.get(document=d2)
        s_d2.last_status_modified_date = march_5
        s_d2.save()

        self.assertTrue(p1_d1.get_overdue_days(self.today) ==
                        0 and p1_d2.get_overdue_days(self.today) == 0)
        self.assertTrue(p1_d1.get_fine(self.today) ==
                        0 and p1_d2.get_fine(self.today) == 0)
        self.assertTrue(s_d1.get_overdue_days(self.today) ==
                        7 and s_d2.get_overdue_days(self.today) == 14)
        self.assertTrue(s_d1.get_fine(self.today) ==
                        700 and s_d2.get_fine(self.today) == 1400)
        self.assertTrue(v_d1.get_overdue_days(self.today) ==
                        21 and v_d2.get_overdue_days(self.today) == 21)
        self.assertTrue(v_d1.get_fine(self.today) ==
                        2100 and v_d2.get_fine(self.today) == 1700)

        print("Test UsersCheckOutDocumentsLibrarianChecksDue_DueDateAndFineIsCorrect was passed successfully")

    def test_UsersRenewsDocumentsLibrarianChecksDueDate_DueDatesAreCorrect(self):
        """Test Case 3
            Initial State:      p1 checked out d1 on March 29.
                                s checked out d2 on March 29.
                                v checked out d2 on March 29

            Action:             p1 renews d1
                                s renews d2
                                v renews d2
                                The librarian checks the information  of  patrons p1, s and v

            Expected Result:    Assuming  that  the  action  happened  self.today  (April 2nd),the information should be
                                p1 (doc., due date):[(d1, April 30th)]
                                s (doc., due date):[(d2, April 16th)]
                                v (doc., due date):(d2, April 9th)]
        """

        p1 = User.objects.all().get(username="p1")
        v = User.objects.all().get(username="v")
        s = User.objects.all().get(username="s")
        d1 = Book.objects.all().get(title="Introduction to Algorithms")
        d2 = Book.objects.all().get(
            title="Design Patterns: Elements of Reusable Object-Oriented Software")

        p1.profile.checkout_document(d1)
        d1.created_date = datetime.datetime(
            year=2018, month=3, day=29, tzinfo=pytz.UTC)
        d1.last_status_modified_date = d1.created_date
        d1.save()

        s.profile.checkout_document(d2)
        d2.created_date = datetime.datetime(
            year=2018, month=3, day=29, tzinfo=pytz.UTC)
        d2.last_status_modified_date = d2.created_date
        d2.save()

        v.profile.checkout_document(d2)
        d2.created_date = datetime.datetime(
            year=2018, month=3, day=29, tzinfo=pytz.UTC)
        d2.save()

        p1_d1 = Record.objects.get(user=p1, document=d1)
        p1_d1.save()

        s_d2 = Record.objects.get(user=s, document=d2)
        p1_d1.save()

        v_d2 = Record.objects.get(user=v, document=d2)
        v_d2.save()

        p1_d1.status = 'TK'
        p1_d1.save()

        p1_d1.last_status_modified_date = self.today  # Because it isn't today
        p1_d1.save()

        s_d2.status = 'TK'
        s_d2.save()

        s_d2.last_status_modified_date = self.today  # Because it isn't today
        s_d2.save()

        v_d2.status = 'TK'
        v_d2.save()

        v_d2.last_status_modified_date = self.today  # Because it isn't today
        v_d2.save()

        p1.profile.renew_document(d1)
        s.profile.renew_document(d2)
        v.profile.renew_document(d2)

        self.assertTrue(p1_d1.get_return_date() == datetime.datetime(
            year=2018, month=4, day=30, tzinfo=pytz.UTC))
        self.assertTrue(s_d2.get_return_date() == datetime.datetime(
            year=2018, month=4, day=16, tzinfo=pytz.UTC))
        self.assertTrue(v_d2.get_return_date() == datetime.datetime(
            year=2018, month=4, day=9, tzinfo=pytz.UTC))

    def test_LibrarianPlacesOutReqUsersRenewsDocumentsLibrarianChecksDueDate_DueDatesAreCorrect(self):
        """Test Case 4
            Initial State:      p1 checked out d1 on March 29.
                                s checked out d2 on March 29.
                                v checked out d2 on March 29

            Action:             The librarian places an outstanding request on document d2
                                p1 renews d1
                                s renews d2
                                v renews d2
                                The librarian checks the information  of  patrons p1, s and v

            Expected Result:    Assuming  that  the  action  happened  today  (April 2nd),the information should be
                                p1 (doc., due date):[(d1, April 30th)]
                                s (doc., due date):[(d2, April 2nd)]
                                v (doc., due date):(d2, April 2nd)]
        """

        p1 = User.objects.all().get(username="p1")
        v = User.objects.all().get(username="v")
        s = User.objects.all().get(username="s")
        d1 = Book.objects.all().get(title="Introduction to Algorithms")
        d2 = Book.objects.all().get(
            title="Design Patterns: Elements of Reusable Object-Oriented Software")
        march_29 = datetime.datetime(year=2018, month=3, day=29, tzinfo=pytz.UTC)

        t = p1.profile.checkout_document(d1)
        t.created_date = march_29
        t.save

        t = s.profile.checkout_document(d2)
        t.created_date = march_29
        t.save
        t = v.profile.checkout_document(d2)
        t.created_date = march_29
        t.save

        p1_d1 = Record.objects.get(user=p1, document=d1)
        s_d2 = Record.objects.get(user=s, document=d2)
        v_d2 = Record.objects.get(user=v, document=d2)

        request = Request.objects.create(user=p1, document=d2, type='OR')
        request.created_date = self.today
        request.save()

        p1_d1.status = 'TK'
        p1_d1.save()

        p1_d1.last_status_modified_date = self.today
        p1_d1.save()

        s_d2.status = 'TK'
        s_d2.save()

        s_d2.last_status_modified_date = self.today
        s_d2.save()

        v_d2.status = 'TK'
        v_d2.save()

        v_d2.last_status_modified_date = self.today
        v_d2.save()

        p1.profile.renew_document(d1)
        s.profile.renew_document(d2)
        v.profile.renew_document(d2)

        self.assertTrue(p1_d1.get_return_date() == datetime.datetime(
            year=2018, month=4, day=30, tzinfo=pytz.UTC))
        self.assertTrue(s_d2.get_return_date() == datetime.datetime(
            year=2018, month=4, day=2, tzinfo=pytz.UTC))
        self.assertTrue(v_d2.get_return_date() == datetime.datetime(
            year=2018, month=4, day=2, tzinfo=pytz.UTC))
        self.assertTrue(p1_d1.get_return_date() == datetime.datetime(
            year=2018, month=4, day=30, tzinfo=pytz.UTC))
        self.assertTrue(s_d2.get_return_date() == datetime.datetime(
            year=2018, month=4, day=2, tzinfo=pytz.UTC))
        self.assertTrue(v_d2.get_return_date() == datetime.datetime(
            year=2018, month=4, day=2, tzinfo=pytz.UTC))

    def test_UsersCheckOutBooksLibrarianCheckWaitingList_WaitingListIsCorrect(self):
        """TC5.
            Initial State:

            Action:             p1 checks out d3
                                s checks out d3
                                v checks out d3
                                The librarian checks the waiting lists for document d3.

            Expected Result:    d3 waiting list:[v]
        """
        print("Test test_UsersCheckOutBooksLibrarianCheckWaitingList_WaitingListIsCorrect begins")

        self.p1.profile.checkout_document(self.d3)
        self.s.profile.checkout_document(self.d3)
        self.v.profile.checkout_document(self.d3)
        p1_d3 = Record.objects.get(user=self.p1, document=self.d3)
        s_d3 = Record.objects.get(user=self.s, document=self.d3)
        v_d3 = Record.objects.get(user=self.v, document=self.d3)
        p1_d3.status = 'TK'
        s_d3.status = 'TK'
        p1_d3.save()
        s_d3.save()

        self.assertTrue(get_waiting_list(
            Record.objects.filter(document=self.d3)) == [v_d3])
        print("Test test_UsersCheckOutBooksLibrarianCheckWaitingList_WaitingListIsCorrect passed successfully")

    def test_waiting_list_on_one_document(self):
        """ TC6.
        Initial State:

        Action:
                i. p1 checks out d3.
                ii. p2 checks out d3.
                iii. s checks out d3.
                iv. v checks out d3.
                v. p3 checks out d3.
                vi. The librarian checks the waiting lists for for document d3.
        Expected Result:
                waiting list: [s, v, p3]
        """
        print("Test test_waiting_list_on_one_document started")

        t = self.p1.profile.checkout_document(self.d3)
        t.created_date = self.today
        t = self.p2.profile.checkout_document(self.d3)
        t.created_date = self.today
        t = self.s.profile.checkout_document(self.d3)
        t.created_date = self.today
        t = self.v.profile.checkout_document(self.d3)
        t.created_date = self.today
        t = self.p3.profile.checkout_document(self.d3)
        t.created_date = self.today

        p1_d3 = Record.objects.get(user=self.p1, document=self.d3)
        s_d3 = Record.objects.get(user=self.s, document=self.d3)
        v_d3 = Record.objects.get(user=self.v, document=self.d3)
        p2_d3 = Record.objects.get(user=self.p2, document=self.d3)
        p3_d3 = Record.objects.get(user=self.p3, document=self.d3)
        p1_d3.status = 'TK'
        p2_d3.status = 'TK'
        p1_d3.save()
        p2_d3.save()

        self.assertTrue(get_waiting_list(Record.objects.filter(
            document=self.d3)) == [s_d3, v_d3, p3_d3])
        print("Test test_waiting_list_on_one_document passed successfully")

    def test_outstanding_request_on_document(self):
        """
        TC7.
        Initial State:
                Run TC6
        Action:
                The librarian places an outstanding request on document d3
        Expected Result:
                i. Waiting list for document d3 is empty.
                ii. p1 and p2 are notified to return the respective
                books.
                iii. s, v and p3 are notified that document d3 is
                not longer available and that they have been
                removed from the waiting list.
        """
        self.test_waiting_list_on_one_document()  # TC6
        Request.objects.create(user=self.librarian, document=self.d3, type='OR')
        self.assertTrue(len(Request.objects.filter(
            user=self.librarian, document=self.d3, type='OR')) == 1)
        self.assertTrue(len(get_waiting_list(
            Record.objects.filter(document=self.d3))) == 0)
        self.assertTrue(len(Record.objects.filter(
            document=self.d3, status='DF')) == 3)

    def test_return_document_and_notify_people_in_waiting_list(self):
        """
        TC8.
        Initial State:
                Run TC6
        Action:
                p2 returns document d3
        Expected Result:
                i. s is notified that document d3 is now available.
                ii. Information of p2 is
                (doc., due date): []
                iii. Waiting list of d3 is
                waiting list: [s, v, p3]
        """
        self.test_waiting_list_on_one_document()  # TC6

        self.p2.profile.return_document(self.d3)

        p2_d3 = Record.objects.get(user=self.p2, document=self.d3)

        p2_d3.status = 'RD'
        p2_d3.save()

        requests = Request.objects.all().filter(
            user=self.s, document=self.d3, is_open=True, type='BA')
        self.assertTrue(len(requests) > 0)

        p2_records = Record.objects.all().filter(user=self.p2, status='TK')

        self.assertTrue(len(p2_records) == 0)

        waiting_list = get_waiting_list(
            Record.objects.all().filter(document=self.d3))

        p3_d3 = Record.objects.get(user=self.p3, document=self.d3)
        s_d3 = Record.objects.get(user=self.s, document=self.d3)
        v_d3 = Record.objects.get(user=self.v, document=self.d3)

        self.assertTrue(waiting_list == [s_d3, v_d3, p3_d3])

    def test_renews_document_and_waiting_list_does_not_change(self):
        """
        T9.
        Initial State:
                Run TC6
        Action:
                p1 renews document d3
        Expected Result:
                i. Information of p1 is
                (doc., due date): [(d3, April 30th)]
                ii. Waiting list of d3 is
                waiting list: [s, v, p3]
        """
        self.test_waiting_list_on_one_document()  # TC6
        self.p1.profile.renew_document(self.d3)

        p1_d3 = Record.objects.get(user=self.p1, document=self.d3)
        p1_d3.last_status_modified_date = self.today
        p1_d3.save()

        s_d3 = Record.objects.get(user=self.s, document=self.d3)
        v_d3 = Record.objects.get(user=self.v, document=self.d3)
        p3_d3 = Record.objects.get(user=self.p3, document=self.d3)

        self.assertTrue(p1_d3.get_return_date() == datetime.datetime(
            year=2018, month=4, day=30, tzinfo=pytz.UTC))
        self.assertTrue(get_waiting_list(Record.objects.filter(
            document=self.d3)) == [s_d3, v_d3, p3_d3])

    def test_checks_out_and_renews_document_renews_applied_only_once(self):
        """
        T10.
        Initial State:
                i. p1 checks out d1 on March 26th.
                ii. p1 renews d1 on March 29th.
                iii. v checks out d1 on March 26th.
                iv. v renews d1 on March 29th.
        Action:
                i. p1 renews document d1.
                ii. v renews document d1.
                iii. The librarian checks the information
                of patrons p1 and v.
        Expected Result:
                p1
                (doc., due date): [(d1, April 26th)]
                v
                (doc., due date): [(d1, April 5th)]
        """
        # region Initial State
        d1_p1 = self.p1.profile.checkout_document(self.d1)
        d1_p1.created_date = datetime.datetime(year=2018, month=3, day=26)

        self.p1.profile.renew_document(self.d1)
        d1_p1.last_status_modified_date = datetime.datetime(
            year=2018, month=3, day=29)
        d1_p1.save()

        d1_v = self.v.profile.checkout_document(self.d1)
        d1_v.created_date = datetime.datetime(year=2018, month=3, day=26)

        self.v.profile.renew_document(self.d1)
        d1_v = Record.objects.all().get(user=self.v, document=self.d1)
        d1_v.last_status_modified_date = datetime.datetime(
            year=2018, month=3, day=29)
        d1_v.save()
        # endregion Initial State

        self.assertEquals(d1_p1.get_return_date().date(),
                          datetime.datetime(year=2018, month=4, day=26).date())
        self.assertEquals(d1_v.get_return_date().date(),
                          datetime.datetime(year=2018, month=4, day=5).date())
