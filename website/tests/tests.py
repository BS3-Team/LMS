# from django.test import TestCase
# from website.models import *
# from django.utils import timezone
# import datetime
#
#
# # Create your tests here.
#
#
# class ModelsTestSuits(TestCase):
#     """Test cases for models"""
#
#     # Set up of our models
#     def setUp(self):
#         Author.objects.create(first_name="David", last_name="Patterson", created_date=datetime.datetime.now())
#
#         Publisher.objects.create(title="MIT", created_date=datetime.datetime.now())
#
#         Journal.objects.create(title="Computer Design", created_date=datetime.datetime.now(),
#                                published_date=datetime.datetime.now(), publisher=Publisher.objects.all()[0],
#                                issue=1)
#         Journal.objects.all()[0].editors.set([Author.objects.all()[0]])
#
#         Document.objects.create(title="Some Project Statements", created_date=datetime.datetime.now(),
#                                 description="Very complicated description with a lot of inconsistencies",
#                                 published_date=datetime.datetime.now())
#         Document.objects.all()[0].authors.set([Author.objects.all()[0]])
#
#         AV.objects.create(title="Home movies", created_date=datetime.datetime.now(),
#                           description="Life of an ordinary man",
#                           published_date=datetime.datetime.now())
#         AV.objects.all()[0].authors.set([Author.objects.all()[0]])
#
#         JournalArticles.objects.create(title="Some Project Statements", created_date=datetime.datetime.now(),
#                                        description="Very complicated description with a lot of inconsistencies",
#                                        published_date=datetime.datetime.now(),
#                                        journal=Journal.objects.all()[0])
#         JournalArticles.objects.all()[0].authors.set([Author.objects.all()[0]])
#
#         Book.objects.create(title="Computer Architecture", created_date=datetime.datetime.now(),
#                             description="Sample description",
#                             published_date=datetime.datetime.now(), publisher=Publisher.objects.all()[0],
#                             issue=1)
#         Book.objects.all()[0].authors.set([Author.objects.all()[0]])
#
#     def test_objects_fields(self):
#         for author in Author.objects.all():
#             self.assertFalse(author.first_name is None or author.first_name == "")
#             self.assertFalse(author.last_name is None or author.last_name == "")
#
#         for publisher in Publisher.objects.all():
#             self.assertFalse(publisher.title is None or publisher.title == "")
#
#         for journal in Journal.objects.all():
#             self.assertFalse(journal.title is None or journal.title == "")
#             self.assertFalse(journal.publisher is None)
#             self.assertFalse(journal.editors is None and not len(journal.editors))
#             self.assertTrue(journal.issue > 0)
#
#         for document in Document.objects.all():
#             self.assertFalse(document.title is None or document.title == "")
#             self.assertFalse(document.description is None or document.description == "")
#             self.assertFalse(document.authors is None and not len(document.authors))
#
#         for av in AV.objects.all():
#             self.assertFalse(av.title is None or av.title == "")
#             self.assertFalse(av.description is None or av.description == "")
#             self.assertFalse(av.authors is None and not len(av.authors))
#
#         for article in JournalArticles.objects.all():
#             self.assertFalse(article.title is None or article.title == "")
#             self.assertFalse(article.description is None or article.description == "")
#             self.assertFalse(article.journal is None)
#             self.assertFalse(article.authors is None and not len(article.authors))
#
#         for book in Book.objects.all():
#             self.assertFalse(book.title is None or book.title == "")
#             self.assertFalse(book.description is None or book.description == "")
#             self.assertFalse(book.publisher is None)
#             self.assertFalse(book.authors is None and not len(book.authors))
#             self.assertTrue(book.issue > 0)
#
#     def test_time(self):
#         for author in Author.objects.all():
#             self.assertTrue(author.created_date <= datetime.datetime.now(tz=timezone.utc))
#
#         for publisher in Publisher.objects.all():
#             self.assertTrue(publisher.created_date <= datetime.datetime.now(tz=timezone.utc))
#
#         for journal in Journal.objects.all():
#             self.assertTrue(journal.created_date <= datetime.datetime.now(tz=timezone.utc))
#             self.assertTrue(journal.published_date <= datetime.datetime.now(tz=timezone.utc))
#
#         for document in Document.objects.all():
#             self.assertTrue(document.created_date <= datetime.datetime.now(tz=timezone.utc))
#             self.assertTrue(document.published_date <= datetime.datetime.now(tz=timezone.utc))
#
#         for av in AV.objects.all():
#             self.assertTrue(av.created_date <= datetime.datetime.now(tz=timezone.utc))
#             self.assertTrue(av.published_date <= datetime.datetime.now(tz=timezone.utc))
#
#         for article in JournalArticles.objects.all():
#             self.assertTrue(article.created_date <= datetime.datetime.now(tz=timezone.utc))
#             self.assertTrue(article.published_date <= datetime.datetime.now(tz=timezone.utc))
#
#         for book in Book.objects.all():
#             self.assertTrue(book.created_date <= datetime.datetime.now(tz=timezone.utc))
#             self.assertTrue(book.published_date <= datetime.datetime.now(tz=timezone.utc))
