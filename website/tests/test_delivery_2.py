import pytz
from django.contrib.auth.models import User, Group
from django.db.models import Q
from django.test import TestCase
from django.utils import timezone
import datetime

from ..models.author import Author
from ..models.publisher import Publisher
from ..models.book import Book
from ..models.av import AV
from ..models.document import Document
from ..models.record import Record

from ..utils import documents_copies_number

"""
All tests should be in package named (module_name)/tests/...
Django will automatically start every test which starts with test*.py
And execute every test which starts with "test"

To start your tests you need to write
"./manage.py test *name_of_testings*" in your docker's bash
(To enter docker bash write in main directory "docker exec -it lms_python_1 bash")
Sometimes you need to explicitly write
$  python3 ./manage.py test

For some reason django couldn't find tests in website so you need to write
$  manage.py test website.tests

To test something specifically write
$  ./manage.py test website.tests.*class_name*.*test_name*


You can specify a custom filename pattern match using the -p (or --pattern) option,
if your test files are named differently from the test*.py pattern:

$ ./manage.py test --pattern="tests_*.py"

You can prevent the test databases from being destroyed by using the "test --keepdb" option.

As long as your tests are properly isolated,
you can run them in parallel to gain a speed up on multi-core hardware. Use test --parallel.

Functions used
setUpTestData: Run once to set up non-modified data for all class methods.
setUp:  Run once for every test method to setup clean data.
tearDown:  Clean up run after every test method.

All tests should be named this way:
[test_MethodName_StateUnderTest_ExpectedBehavior]
If you want your test to run with other you MUST write "test" at the beginning of the your test


The idea of naming them like this was took from Roy Osherove
Source: http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
"""


class TestSuiteDelivery2(TestCase):
    """This module has unit tests specified in test cases for first delivery"""

    # def setUp(self):

    # test case 1
    def test_setup_EmptySystem_UsersAndDocumentsAreCreated(self):
        """This test creates 3 copies of book b1, 2 of b2, 1 of b3, 2 AV: av1, av2, patrons p1,p2,p3, librarian"""
        print("Test test_setup_EmptySystem_UsersAndDocumentsAreCreated begins")

        authors = []
        docs = []
        self.today = datetime.datetime(year=2018, month=3, day=5, tzinfo=pytz.UTC)

        # create b1 - docs[0]
        authors.append(Author.objects.create(
            first_name="Thomas", last_name="H. Cormen"))
        authors.append(Author.objects.create(
            first_name="Charles", last_name="E. Leiserson"))
        authors.append(Author.objects.create(
            first_name="Ronald", last_name="L. Rivest"))
        authors.append(Author.objects.create(
            first_name="Clifford", last_name="Stein"))

        p = Publisher.objects.create(
            title="MIT Press", created_date=datetime.datetime.now(tz=timezone.utc))

        docs.append(Book.objects.create(title="Introduction to Algorithms",
                                        published_date=datetime.datetime(
                                            year=2009, month=1, day=1, tzinfo=pytz.UTC),
                                        publisher=p,
                                        issue=3, copies=3))

        docs[0].authors.set(authors)

        authors.clear()

        # create b2 - docs[1]

        authors.append(Author.objects.create(
            first_name="Erich", last_name="Gamma"))
        authors.append(Author.objects.create(
            first_name="Ralph", last_name="Johnson"))
        authors.append(Author.objects.create(
            first_name="John", last_name="Vlissides"))
        authors.append(Author.objects.create(
            first_name="Richard", last_name="Helm"))

        p = Publisher.objects.create(title="Addison-Wesley Professional")

        docs.append(Book.objects.create(title="Design Patterns: Elements of Reusable Object-Oriented Software",
                                        is_bestseller=True,
                                        published_date=datetime.datetime(
                                            year=2003, month=1, day=1, tzinfo=pytz.UTC),
                                        publisher=p,
                                        issue=1, copies=2))
        docs[1].authors.set(authors)

        authors.clear()

        # create b3 - docs[2]

        authors.append(Author.objects.create(
            first_name="Brooks", last_name="Jr"))
        authors.append(Author.objects.create(
            first_name="Frederick", last_name="P."))

        p = Publisher.objects.create(
            title="Addison-Wesley Longman Publishing Co., Inc.")

        docs.append(Book.objects.create(title="The Mythical Man-month",
                                        is_reference=True,
                                        published_date=datetime.datetime(
                                            year=1995, month=1, day=1, tzinfo=pytz.UTC),
                                        publisher=p,
                                        issue=2, copies=1))
        docs[2].authors.set(authors)
        authors.clear()

        # create av1 - docs[3]

        authors.append(Author.objects.create(
            first_name="Tony", last_name="Hoare"))
        docs.append(AV.objects.create(
            title="Null References: The Billion Dollar Mistake", copies=1, type='AV'))
        docs[3].authors.set(authors)
        authors.clear()

        # create av2 - docs[4]

        authors.append(Author.objects.create(
            first_name="Claude", last_name="Shannon"))
        docs.append(AV.objects.create(title="Information Entropy", copies=1))
        docs[4].authors.set(authors)
        authors.clear()

        # create p1 - patron
        user = User.objects.create(
            username="p1", first_name="Sergey", last_name="Afonso", password="12345")
        user.groups.add(Group.objects.get(name="faculty"))
        user.profile.address = "Via Margutta, 3"
        user.profile.phone_number = "30001"
        user.save()

        # create p2 - patron
        user = User.objects.create(
            username="p2", first_name="Nadia", last_name="Teixeira", password="12345")
        user.groups.add(Group.objects.get(name="student"))
        user.profile.address = "Via Sacra, 13"
        user.profile.phone_number = "30002"
        user.save()

        # create p3 - patron
        user = User.objects.create(
            username="p3", first_name="Elvira", last_name="Espindola", password="12345")
        user.groups.add(Group.objects.get(name="student"))
        user.profile.address = "Via del Corso, 22"
        user.profile.phone_number = "30003"
        user.save()

        # create librarian
        user = User.objects.create(username="librarian1", password="12345")
        user.groups.add(Group.objects.get(name="librarian"))

        self.assertTrue(documents_copies_number(Document.objects.all()) == 8 and
                        len(User.objects.all()) == 4)

        print("Test test_setup_EmptySystem_UsersAndDocumentsAreCreated was passed successfully")

    # test case 2
    def test_UsersAndDocumentsAreCreated_UsersAndDocumentsAreChanged(self):
        print("Test test_UsersAndDocumentsAreCreated_UsersAndDocumentsAreChanged begins")

        self.test_setup_EmptySystem_UsersAndDocumentsAreCreated()
        b1 = Document.objects.all().get(title="Introduction to Algorithms")
        b3 = Document.objects.all().get(title="The Mythical Man-month")
        b1.copies -= 2
        b3.copies -= 1
        b1.save()
        b3.save()

        User.objects.all().get(username="p2").delete()

        self.assertTrue(documents_copies_number(Document.objects.all()) == 5 and
                        len(User.objects.all()) == 3)

        print("Test test_UsersAndDocumentsAreCreated_UsersAndDocumentsAreChanged was passed successfully")

    # test case 3
    def test_UsersAndDocumentsAreCreated_InformationIsChecked(self):
        print("Test test_UsersAndDocumentsAreCreated_InformationIsChecked begins")

        self.test_setup_EmptySystem_UsersAndDocumentsAreCreated()

        p1 = User.objects.all().get(username="p1")
        p3 = User.objects.all().get(username="p3")

        self.assertTrue(p1.first_name == "Sergey" and p1.last_name == "Afonso")
        self.assertTrue(p1.profile.address == "Via Margutta, 3")
        self.assertTrue(p1.profile.phone_number == "30001")
        self.assertTrue(p1.groups.all()[0] ==
                        Group.objects.get(name="faculty"))
        self.assertTrue(len(Record.objects.all().filter(user=p1)) == 0)

        self.assertTrue(p3.first_name ==
                        "Elvira" and p3.last_name == "Espindola")
        self.assertTrue(p3.profile.address == "Via del Corso, 22")
        self.assertTrue(p3.profile.phone_number == "30003")
        self.assertTrue(p3.groups.all()[0] ==
                        Group.objects.get(name="student"))
        self.assertTrue(len(Record.objects.all().filter(user=p3)) == 0)

        print("Test test_UsersAndDocumentsAreCreated_InformationIsChecked was passed successfully")

    # test case 4
    def test_UsersAndDocumentsAreChanged_InformationIsCheckedPartially(self):
        print("Test test_UsersAndDocumentsAreChanged_InformationIsCheckedPartially begins")

        self.test_UsersAndDocumentsAreCreated_UsersAndDocumentsAreChanged()
        p2 = User.objects.all().filter(username="p2")
        p3 = User.objects.all().get(username="p3")

        self.assertTrue(len(p2) == 0)

        self.assertTrue(p3.first_name ==
                        "Elvira" and p3.last_name == "Espindola")
        self.assertTrue(p3.profile.address == "Via del Corso, 22")
        self.assertTrue(p3.profile.phone_number == "30003")
        self.assertTrue(p3.groups.all()[0] ==
                        Group.objects.get(name="student"))
        self.assertTrue(len(Record.objects.all().filter(user=p3)) == 0)

        print("Test test_UsersAndDocumentsAreChanged_InformationIsCheckedPartially was passed successfully")

    # test case 5
    def test_UsersAndDocumentsAreChanged_DeletedPatronCannotCheckout(self):
        self.test_UsersAndDocumentsAreCreated_UsersAndDocumentsAreChanged()
        b1 = Document.objects.all().filter(
            title="Introduction to Algorithms")[0]
        try:
            b1.checkout_document(User.objects.all().filter(username="p2")[0])
        except Exception:
            checked_out = False
        else:
            checked_out = True

        self.assertFalse(checked_out)

    # test case 6
    def test_UsersAndDocumentsAreChanged_DocumentsAreCheckedOutAndInformationIsChecked(self):
        self.test_UsersAndDocumentsAreCreated_UsersAndDocumentsAreChanged()

        p1 = User.objects.all().get(username="p1")
        p3 = User.objects.all().get(username="p3")
        b1 = Document.objects.all().get(title="Introduction to Algorithms")
        b2 = Document.objects.all().get(
            title="Design Patterns: Elements of Reusable Object-Oriented Software")

        t = p1.profile.checkout_document(b1)
        t.created_date = self.today
        t.save()

        t = p3.profile.checkout_document(b1)
        t.created_date = self.today
        t.save()

        t = p3.profile.checkout_document(b2)
        t.created_date = self.today
        t.save()

        rp1 = Record.objects.all().filter(user=p1).first()
        rp3 = Record.objects.all().filter(user=p3, document=b2).first()

        self.assertTrue(p1.first_name == "Sergey" and p1.last_name == "Afonso")
        self.assertTrue(p1.profile.address == "Via Margutta, 3")
        self.assertTrue(p1.profile.phone_number == "30001")
        self.assertTrue(p1.groups.all()[0] ==
                        Group.objects.get(name="faculty"))
        rp1.last_status_modified_date = self.today
        rp3.last_status_modified_date = self.today
        rp1.save()
        rp3.save()
        self.assertTrue(
            rp1.document == b1 and rp1.get_return_date().year == 2018 and
            rp1.get_return_date().month == 4 and rp1.get_return_date().day == 2)

        self.assertTrue(p3.first_name ==
                        "Elvira" and p3.last_name == "Espindola")
        self.assertTrue(p3.profile.address == "Via del Corso, 22")
        self.assertTrue(p3.profile.phone_number == "30003")
        self.assertTrue(p3.groups.all()[0] ==
                        Group.objects.get(name="student"))
        self.assertTrue(
            rp3.document == b2 and rp3.get_return_date().year == 2018 and
            rp3.get_return_date().month == 3 and rp3.get_return_date().day == 19)

    # test case 7
    def test_UsersAndDocumentsAreCreated_DocumentsAreCheckedOutAndInformationIsChecked(self):
        self.test_setup_EmptySystem_UsersAndDocumentsAreCreated()

        p1 = User.objects.all().get(username="p1")
        p2 = User.objects.all().get(username="p2")

        b1 = Document.objects.all().get(title="Introduction to Algorithms")
        b2 = Document.objects.all().get(
            title="Design Patterns: Elements of Reusable Object-Oriented Software")
        b3 = Document.objects.all().get(title="The Mythical Man-month")

        av1 = Document.objects.all().get(
            title="Null References: The Billion Dollar Mistake")
        av2 = Document.objects.all().get(title="Information Entropy")

        current_date = self.today
        t = p1.profile.checkout_document(b1)
        t.last_status_modified_date = current_date

        t = p1.profile.checkout_document(b2)
        t.last_status_modified_date = current_date

        try:
            t = p1.profile.checkout_document(b3)
            t.last_status_modified_date = current_date
        except AttributeError:
            pass

        t = p1.profile.checkout_document(av1)
        t.last_status_modified_date = current_date

        t = p2.profile.checkout_document(b1)
        t.last_status_modified_date = current_date

        t = p2.profile.checkout_document(b2)
        t.last_status_modified_date = current_date

        t = p2.profile.checkout_document(av2)
        t.last_status_modified_date = current_date

        rp1 = Record.objects.all().filter(user=p1).order_by("pk")

        rp2 = Record.objects.all().filter(user=p2).order_by("pk")

        self.assertTrue(p1.first_name == "Sergey" and p1.last_name == "Afonso")
        self.assertTrue(p1.profile.address == "Via Margutta, 3")
        self.assertTrue(p1.profile.phone_number == "30001")
        self.assertTrue(p1.groups.all()[0] ==
                        Group.objects.get(name="faculty"))
        rp1_0 = rp1[0]
        rp1_1 = rp1[1]
        rp1_2 = rp1[2]
        rp1_0.last_status_modified_date = self.today
        rp1_1.last_status_modified_date = self.today
        rp1_2.last_status_modified_date = self.today
        rp1_0.save()
        rp1_1.save()
        rp1_2.save()
        self.assertTrue(
            rp1_0.document == b1 and rp1_0.get_return_date().year == 2018 and
            rp1_0.get_return_date().month == 4 and rp1_0.get_return_date().day == 2)
        self.assertTrue(
            rp1_1.document == b2 and rp1_1.get_return_date().year == 2018 and
            rp1_1.get_return_date().month == 4 and rp1_1.get_return_date().day == 2)

        self.assertTrue(
            rp1_2.document == av1 and rp1_2.get_return_date().year == 2018 and
            rp1_2.get_return_date().month == 3 and rp1_2.get_return_date().day == 19)

        self.assertTrue(p2.first_name ==
                        "Nadia" and p2.last_name == "Teixeira")
        self.assertTrue(p2.profile.address == "Via Sacra, 13")
        self.assertTrue(p2.profile.phone_number == "30002")
        self.assertTrue(p2.groups.all()[0] ==
                        Group.objects.get(name="student"))

        for i in rp2:
            i.last_status_modified_date = self.today
            i.save()

        self.assertTrue(
            rp2[0].document == b1 and rp2[0].get_return_date().year == 2018 and
            rp2[0].get_return_date().month == 3 and rp2[0].get_return_date().day == 26)
        self.assertTrue(
            rp2[1].document == b2 and rp2[1].get_return_date().year == 2018 and
            rp2[1].get_return_date().month == 3 and rp2[1].get_return_date().day == 19)
        self.assertTrue(
            rp2[2].document == av2 and rp2[2].get_return_date().year == 2018 and
            rp2[2].get_return_date().month == 3 and rp2[2].get_return_date().day == 26)

    # test case 8
    def test_UsersAreCreatedAndDocumentsAreCheckedOut_DueDatesAreChecked(self):
        self.test_setup_EmptySystem_UsersAndDocumentsAreCreated()

        p1 = User.objects.all().get(username="p1")
        p2 = User.objects.all().get(username="p2")

        b1 = Document.objects.all().get(title="Introduction to Algorithms")
        b2 = Document.objects.all().get(
            title="Design Patterns: Elements of Reusable Object-Oriented Software")

        av1 = Document.objects.all().get(
            title="Null References: The Billion Dollar Mistake")

        t = p1.profile.checkout_document(b1)
        t.created_date=datetime.datetime(year=2018, month=2, day=9, tzinfo=pytz.UTC)
        t.status = Record.TAKEN
        t.save()

        t = p1.profile.checkout_document(b2)
        t.created_date = datetime.datetime(year=2018, month=2, day=2, tzinfo=pytz.UTC)
        t.status = Record.TAKEN
        t.save()

        t = p2.profile.checkout_document(b1)
        t.created_date = datetime.datetime(year=2018, month=2, day=5, tzinfo=pytz.UTC)
        t.status = Record.TAKEN
        t.save()

        t = p2.profile.checkout_document(av1)
        t.created_date = datetime.datetime(year=2018, month=2, day=17, tzinfo=pytz.UTC)
        t.status = Record.TAKEN
        t.save()

        rp1 = Record.objects.all().filter((Q(user=p1, status='TK') | Q(user=p1, status='RN'))).order_by("pk")
        rp2 = Record.objects.all().filter((Q(user=p2, status='TK') | Q(user=p2, status='RN'))).order_by("pk")

        rp1_1 = rp1[1]
        rp1_1.last_status_modified_date = datetime.datetime(
            year=2018, month=2, day=2, tzinfo=pytz.UTC)
        rp1_1.save()

        rp2_0 = rp2[0]
        rp2_0.last_status_modified_date = datetime.datetime(
            year=2018, month=2, day=5, tzinfo=pytz.UTC)
        rp2_0.save()

        rp2_1 = rp2[1]
        rp2_1.last_status_modified_date = datetime.datetime(
            year=2018, month=2, day=17, tzinfo=pytz.UTC)
        rp2_1.save()

        self.assertTrue(rp1_1.document == b2)
        self.assertEqual(rp1_1.get_overdue_days(self.today), 3)

        self.assertTrue(rp2_0.document == b1)
        self.assertEqual(rp2_0.get_overdue_days(self.today), 7)

        self.assertTrue(rp2_1.document == av1)
        self.assertEqual(rp2_1.get_overdue_days(self.today), 2)
