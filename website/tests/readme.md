## Tests

All tests should be in package named (module_name)/tests/...
Django will automatically start every test which starts with test*.py
And execute every test which starts with "test"

To start your tests you need to write
"./manage.py test *name_of_testings*" in your docker's bash
To enter docker bash write in main directory
    docker exec -it lms_python_1 bash
To run tests enter

    $  python3 ./lms/manage.py test

Sometimes you need to explicitly write

    $ ./lms/manage.py test website.tests


To test something specifically write

    $  ./manage.py test website.tests.*file_of_the_test_name*.*class_name*.*test_name*


You can specify a custom filename pattern match using the -p (or --pattern) option,
if your test files are named differently from the test*.py pattern:

    $ ./manage.py test --pattern="tests_*.py"

You can prevent the test databases from being destroyed by using the "test --keepdb" option.

As long as your tests are properly isolated,
you can run them in parallel to gain a speed up on multi-core hardware. Use test --parallel.

Functions used
- setUpTestData: Run once to set up non-modified data for all class methods.
- setUp:  Run once for every test method to setup clean data.
- tearDown:  Clean up run after every test method.

In CI tests are run automatically