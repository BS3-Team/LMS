from django.contrib.auth.management.commands import createsuperuser
from django.core.management.base import CommandError


class Command(createsuperuser.Command):
    """Redefenition of createsuperuser command"""
    def handle(self, *args, **options):
        # If there is already a superuser, we throw and exception
        if self.UserModel.objects.filter(is_superuser=True).exists():
            raise CommandError("There is a superuser created already")
        super().handle(*args, **options)
