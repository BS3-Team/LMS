from django.contrib.admin.models import LogEntry, DELETION, CHANGE, ADDITION
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver
from django.utils import timezone
from templated_email import get_templated_mail

from ..models.record import Record
from ..models.request import Request
from ..utils import get_waiting_list


@receiver(post_save, sender=Record)
def create_request_on_return(sender, instance, **kwargs):
    """Send mail to the user on top of the queue"""
    if instance.status == 'RD':
        waiting_list = get_waiting_list(
            Record.objects.filter(document=instance.document), True)
        for record in waiting_list:
            # If there is someone in queue who haven't been notified, creates a request
            if Request.objects.filter(user=record.user, document=record.document, is_open=True).first() is None:
                user_request = Request(
                    user=record.user, document=record.document, type=Request.BOOK_AVAILABLE)
                user_request.save()
                break


@receiver(post_save, sender=Request)
def send_mail_to_user(sender, instance, **kwargs):
    """Send mail to the users who have specific document when request is created"""
    if instance.is_open and not instance.is_sent:
        if instance.type == Request.BOOK_REQUEST:
            for owner in Record.objects.filter(
                    (Q(status=Record.TAKEN) | Q(status=Record.RENEWED)) & Q(document=instance.document)):

                message = get_templated_mail(
                    template_name='requested_back',
                    from_email='systemlibrary@yandex.ru',
                    to=[owner.user.email],
                    context={
                        'first_name': owner.user.first_name,
                        'last_name': owner.user.last_name,
                        'book_name': owner.document.title,
                        'authors': ', '.join([i.__str__() for i in owner.document.authors.all()])
                    },
                )

                sent = message.send(fail_silently=False)

                # If sent - close instance
                if sent:
                    instance.is_open = False
                    instance.is_sent = False
                    instance.save()

        # If book is available
        elif instance.type == Request.BOOK_AVAILABLE:
            user_to_send = instance.user
            message = get_templated_mail(
                template_name='item_available',
                from_email='systemlibrary@yandex.ru',
                to=[user_to_send.email],
                context={
                    'first_name': user_to_send.first_name,
                    'last_name': user_to_send.last_name,
                    'book_name': instance.document.title,
                    'authors': ', '.join([i.__str__() for i in instance.document.authors.all()])
                },
            )

            sent = message.send(fail_silently=False)
            if sent:
                instance.is_sent = True
                instance.save()

        # If outstanding request was created
        elif instance.type == Request.OUTSTANDING_REQUEST:
            # Create requests for those who took and renewed the items
            for owner in Record.objects.filter(
                    (Q(status=Record.TAKEN) | Q(status=Record.RENEWED)) & Q(document=instance.document)):
                request = Request.objects.create(
                    user=owner.user, document=owner.document, type=Request.RETURN_BOOK)
                request.save()

            # Change records of waiting to deferred
            records = Record.objects.filter(
                document=instance.document, status=Record.WAITING)
            for record in records:
                record.status = Record.DEFERRED
                record.save()
                request = Request.objects.create(
                    document=record.document, user=record.user, type=Request.DEFERRED_BOOK)
                request.save()

            # Close every book available requests
            for request in Request.objects.filter(document=instance.document, type=Request.BOOK_AVAILABLE):
                request.is_open = False
                request.save()

        # If it is request to return book
        elif instance.type == Request.RETURN_BOOK:
            user_to_send = instance.user
            message = get_templated_mail(
                template_name='requested_back',
                from_email='systemlibrary@yandex.ru',
                to=[user_to_send.email],
                context={
                    'first_name': user_to_send.first_name,
                    'last_name': user_to_send.last_name,
                    'book_name': instance.document.title,
                    'authors': ', '.join([i.__str__() for i in instance.document.authors.all()])
                },
            )

            sent = message.send(fail_silently=False)

            if sent:
                instance.is_sent = True
                instance.is_open = False
                instance.save()

        # If it is deferred book request
        elif instance.type == Request.DEFERRED_BOOK:
            user_to_send = instance.user
            message = get_templated_mail(
                template_name='deffered_book_notification',
                from_email='systemlibrary@yandex.ru',
                to=[user_to_send.email],
                context={
                    'first_name': user_to_send.first_name,
                    'last_name': user_to_send.last_name,
                    'book_name': instance.document.title,
                    'authors': ', '.join([i.__str__() for i in instance.document.authors.all()])
                },
            )
            sent = message.send(fail_silently=False)

            if sent:
                instance.is_sent = True
                instance.is_open = False
                instance.save()


@receiver(post_save, sender=LogEntry)
def change_message(sender, instance, created, using, **kwargs):
    if not instance.change_message:
        if instance.action_flag == 1:
            instance.change_message = 'Object Created'
        elif instance.action_flag == 3:
            instance.change_message = 'Object Deleted'
        instance.save()
    else:
        entry = LogEntry.objects.filter(object_id=instance.object_id, action_flag=instance.action_flag,
                                        action_time=instance.action_time)
        if entry.exists() and instance.pk > entry.first().pk:
            entry.first().delete()


@receiver(post_save, sender=Request)
def post_save_request(sender, instance, created, using, **kwargs):
    if created:
        if not LogEntry.objects.filter(object_id=instance.pk, action_flag=ADDITION,
                                       action_time=timezone.now()).exists():
            if instance.type == 'OR':
                message = 'Outstanding Request Created'
            else:
                message = 'Message Request Created'
            log_creation(instance, ADDITION, message, user_id=instance.user.id)
    else:
        if not LogEntry.objects.filter(object_id=instance.pk, action_flag=CHANGE,
                                       action_time=timezone.now()).exists():
            if instance.type != 'OR' and instance.is_sent:
                message = 'Message Sent'
            else:
                message = 'Request Changed'
            log_creation(instance, CHANGE, message, user_id=instance.user.id)


@receiver(post_delete, sender=Request)
def post_delete_request(sender, instance, using, **kwargs):
    if not LogEntry.objects.filter(object_id=instance.pk, action_flag=DELETION,
                                   action_time=timezone.now()).exists():
        log_creation(instance, DELETION, 'Request Deleted', user_id=instance.user.id)


@receiver(post_save, sender=Record)
def post_save_handler(sender, instance, created, using, **kwargs):
    if created:
        if not LogEntry.objects.filter(object_id=instance.pk, action_flag=ADDITION,
                                       action_time=timezone.now()).exists():
            log_creation(instance, ADDITION, 'Object Created', user_id=instance.user.id)
    else:
        if not LogEntry.objects.filter(object_id=instance.pk, action_flag=CHANGE, action_time=timezone.now()).exists():
            changes = {"changed": {"fields": []}}
            for (instance_fields, old_state_fields) in zip(vars(instance), vars(instance.old_state)):
                if getattr(instance, instance_fields) != getattr(instance.old_state, old_state_fields):
                    changes["changed"]["fields"].append(instance_fields)
            if changes["changed"]["fields"]:
                log_creation(instance, CHANGE, [changes], user_id=instance.user.id)
                instance.old_state = instance


@receiver(post_delete, sender=Record)
def post_delete_handler(sender, instance, using, **kwargs):
    if not LogEntry.objects.filter(object_id=instance.pk, action_flag=DELETION, action_time=timezone.now()).exists():
        log_creation(instance, DELETION, 'Object Deleted', user_id=instance.user.id)


@receiver(pre_save, sender=User)
def check_for_superuser(sender, instance, using, **kwargs):
    """Check if there is already one superuser"""
    # Protects your house from having more then one superuser
    # In other words kostil'
    super_user = User.objects.filter(is_superuser=True)
    if instance.is_superuser and super_user.exists() and super_user.first() != instance:
        instance.is_superuser = False


def log_creation(object, action_flag, change_message, user_id=1):
    LogEntry.objects.log_action(
        user_id=user_id,
        content_type_id=ContentType.objects.get_for_model(object).pk,
        object_id=object.pk,
        object_repr=str(object),
        action_flag=action_flag,
        change_message=change_message
    )
