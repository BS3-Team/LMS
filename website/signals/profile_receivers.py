from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from ..models.profile import Profile


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    """Creates new profile for every new instance of a user"""
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    """Saves profile when user is saved"""
    instance.profile.save()
